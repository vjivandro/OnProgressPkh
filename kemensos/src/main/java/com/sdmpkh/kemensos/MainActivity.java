package com.sdmpkh.kemensos;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.sdmpkh.kemensos.fragment.LowonganFragment;
import com.sdmpkh.kemensos.fragment.NotifikasiFragment;
import com.sdmpkh.kemensos.fragment.ProfilFragment;
import com.sdmpkh.kemensos.helper.SQLiteHelper;
import com.sdmpkh.kemensos.model.DataKtpModel;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {
    private static final String TAG = MainActivity.class.getSimpleName();
    private Fragment fragment;
    private FragmentManager fragmentManager;
    private FragmentTransaction transaction;
    private int checkedPermission = PackageManager.PERMISSION_DENIED;
    private SharedPreferences sharedPreferences;
    public static final String my_shared_preferences = "my_shared_preferences";

    SQLiteHelper db;

    String kdagama="", kodepos="", nilai="", penyelenggara="", kdpenempatan="", kdtandatangan="", kdbidangilmupelatihan="",
            bidangkerjapraktek="", tahunkerjapraktek="", kdkecamatanpkh="", kdkontrak="", kdparpol="", kdhukum="", namaibu="";
    String tempatLahir, tanggallahir, jk, merried, kebangsaan;
    String idpersonal, nik, nama, email, ponsel, alamat, kd_posisi, posisi, kd_pro, provinsi, kd_kab, kabupaten, kd_kec, kecamatan;
    String namasekolah, thlulus, ipk;
    String pengalaman, tahun_masuk, tahun_keluar;
    String pencapaian, tahun_prestasi;
    String sertifikat, tahun_sertifikat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // from session
        sharedPreferences = getSharedPreferences(my_shared_preferences, Context.MODE_PRIVATE);
        nik = sharedPreferences.getString("nik", "");
        idpersonal = sharedPreferences.getString("idpersonal", "");
        nama = sharedPreferences.getString("nama", "");
        email = sharedPreferences.getString("email", "");
        ponsel = sharedPreferences.getString("hp", "");
        alamat = sharedPreferences.getString("alamat", "");
        kd_posisi = sharedPreferences.getString("kd_posisi", "");
        posisi = sharedPreferences.getString("posisi", "");
        kd_pro = sharedPreferences.getString("kd_pro", "");
        provinsi = sharedPreferences.getString("provinsi", "");
        kd_kab = sharedPreferences.getString("kd_kab", "");
        kabupaten = sharedPreferences.getString("kabupaten", "");
        kd_kec = sharedPreferences.getString("kd_kec", "");
        kecamatan = sharedPreferences.getString("kecamatan", "");

        // from SQLITE
        db = new SQLiteHelper(this);

        // Ktp
        DataKtpModel dataKtpModel = db.getAllDataKtp();
        tempatLahir = dataKtpModel.tempatlahir;
        tanggallahir = dataKtpModel.tanggallahir;
        jk = dataKtpModel.jenisKelamin;
        merried = dataKtpModel.status;
        kebangsaan = dataKtpModel.kebangsaan;


        // permision write external storage
        checkedPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (Build.VERSION.SDK_INT >= 23 && checkedPermission != PackageManager.PERMISSION_GRANTED) {
            requestPermission();
        } else {
            checkedPermission = PackageManager.PERMISSION_GRANTED;
        }

        // permision camera
        checkedPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if (Build.VERSION.SDK_INT >= 23 && checkedPermission != PackageManager.PERMISSION_GRANTED) {
            requestPermission();
        } else {
            checkedPermission = PackageManager.PERMISSION_GRANTED;
        }

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        fragmentManager = getSupportFragmentManager();
        navigation.setOnNavigationItemSelectedListener(this);
        navigation.setSelectedItemId(R.id.navigation_dashboard);
        onNavigationItemSelected(R.id.navigation_dashboard);// iki bang bne select dek dashboard
        getPremission();
    }

    private void onNavigationItemSelected(int item) {
        switch (item) {
            case R.id.navigation_dashboard:
                fragment = new ProfilFragment();
                break;
        }
        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.container, fragment);
            ft.commit();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.navigation_home:
                fragment = new LowonganFragment();
                break;
            case R.id.navigation_dashboard:
                fragment = new ProfilFragment();
                break;
            case R.id.riwayat_lamaran:
                fragment = new NotifikasiFragment();
                break;
        }
        transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.container, fragment).commit();
        return true;
    }

    private void getPremission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Periksa apakah pengguna pernah diminta permission ini dan menolaknya.
            // Jika pernah, kita akan memberikan penjelasan mengapa permission ini dibutuhkan.
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (shouldShowRequestPermissionRationale(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    // Tampilkan penjelasan mengapa aplikasi ini perlu membaca kontak disini
                    // sebelum akhirnya me-request permission dan menampilkan hasilnya
                }
            }

            // Lakukan request untuk meminta permission (menampilkan jendelanya)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},1);
            }
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            // Periksa apakah pengguna pernah diminta permission ini dan menolaknya.
            // Jika pernah, kita akan memberikan penjelasan mengapa permission ini dibutuhkan.
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (shouldShowRequestPermissionRationale(
                        Manifest.permission.CAMERA)) {
                    // Tampilkan penjelasan mengapa aplikasi ini perlu membaca kontak disini
                    // sebelum akhirnya me-request permission dan menampilkan hasilnya
                }
            }

            // Lakukan request untuk meminta permission (menampilkan jendelanya)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.CAMERA},1);
            }
        }
    }

    @SuppressLint("NewApi")
    private void requestPermission() {
        Toast.makeText(MainActivity.this, "Requesting permission", Toast.LENGTH_SHORT).show();
        this.requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA},1);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    checkedPermission = PackageManager.PERMISSION_GRANTED;
                }
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);

        MenuItem item1 = menu.findItem(R.id.action_logout);
        View view = MenuItemCompat.getActionView(item1);

        MenuItem item2 = menu.findItem(R.id.action_notifications);
        View view1 = MenuItemCompat.getActionView(item2);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.action_logout:
                showDialogLogout();
                break;
            case R.id.action_notifications:
                postAllParameter();
                break;
        }


        return super.onOptionsItemSelected(item);
    }

    private void postAllParameter() {

        System.out.println("All Data Probadi => " + idpersonal + " " + nama + " " + nik + " " + alamat + " " + kd_posisi + " "
                + kd_pro + " " + kd_kab + " " + kd_kec + " Param Kosong => " + kdagama + " " + kodepos + " " + nilai + " " +
                penyelenggara + " " + kdpenempatan + " " + kdtandatangan + " " + kdbidangilmupelatihan + " " +
                bidangkerjapraktek + " " + tahunkerjapraktek + " " + kdkecamatanpkh + " " + kdkontrak + " " +
                kdparpol + " " + kdhukum + " " + namaibu + " KTP => " + tempatLahir + " " + tanggallahir + " " +
                jk + " " + merried + " " + kebangsaan + " Pendidikan => " + namasekolah + " " + thlulus + " " + ipk + " Pengalaman kerja => " +
                pengalaman + " " + tahun_masuk + " " + tahun_keluar + " Prestasi => " + pencapaian + " " + tahun_prestasi + " Sertifikat => " +
                sertifikat + " " + tahun_sertifikat);
    }


    private void showDialogLogout(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);
        alertDialogBuilder.setTitle("Keluar dari aplikasi?");
        alertDialogBuilder
                .setMessage("Klik Ya untuk keluar!")
                .setIcon(R.mipmap.ic_launcher)
                .setCancelable(false)
                .setPositiveButton("Ya",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        // jika tombol diklik, maka akan menutup activity ini
                        MainActivity.this.finish();
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putBoolean(LoginActivity.session_status, false);
                        editor.putString("nik", null);
                        editor.clear();
                        editor.commit();

                        Intent inten = new Intent(MainActivity.this, LoginActivity.class);
                        finish();
                        startActivity(inten);

                    }
                })
                .setNegativeButton("Tidak",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // jika tombol ini diklik, akan menutup dialog
                        // dan tidak terjadi apa2
                        dialog.cancel();
                    }
                });

        // membuat alert dialog dari builder
        AlertDialog alertDialog = alertDialogBuilder.create();

        // menampilkan alert dialog
        alertDialog.show();
    }

}
