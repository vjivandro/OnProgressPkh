package com.sdmpkh.kemensos;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.sdmpkh.kemensos.Service.ApiService;
import com.sdmpkh.kemensos.helper.SQLiteHelper;
import com.sdmpkh.kemensos.model.DataPribadiModel;
import com.sdmpkh.kemensos.model.ItemJabatan;
import com.sdmpkh.kemensos.model.ItemKabupaten;
import com.sdmpkh.kemensos.model.ItemKecamatan;
import com.sdmpkh.kemensos.model.ItemProvinsi;
import com.sdmpkh.kemensos.model.ItemRegional;
import com.sdmpkh.kemensos.model.ItemRegister;
import com.sdmpkh.kemensos.model.JabatanModel;
import com.sdmpkh.kemensos.model.KabupatenModel;
import com.sdmpkh.kemensos.model.KecamatanModel;
import com.sdmpkh.kemensos.model.ProvinsiModel;
import com.sdmpkh.kemensos.model.RegionalModel;
import com.sdmpkh.kemensos.utils.Server;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PendaftaranActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, View.OnClickListener {

    private EditText nikTv, namaTv, emailTv, nopeTv, alamatTv;
    private Spinner posisi_spin, reg_spin, prov_spin, kab_spin, kec_spin;
    private Button reg_button;
    private String id, alamat, kd_posisi, kd_reg, kd_pro, kd_kab, kd_kec, posisi,regional, provinsi, kabupaten, kecamatan;
    private ProgressDialog progressDialog;
    private SharedPreferences sharedPreferences;
    private SQLiteHelper db;

    public static final String my_shared_preferences = "my_shared_preferences";

    List<String> listSpinner;
    List<JabatanModel> posisiItem;
    List<RegionalModel> regionalItem;
    List<ProvinsiModel> provinsiItem;
    List<KabupatenModel> kabupatenItem;
    List<KecamatanModel> kecamatanItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pendaftaran);

        sharedPreferences = getSharedPreferences(my_shared_preferences, Context.MODE_PRIVATE);
        progressDialog = new ProgressDialog(this);

        nikTv = (EditText) findViewById(R.id.et_nik);
        namaTv = (EditText) findViewById(R.id.et_nama);
        emailTv = (EditText) findViewById(R.id.et_email);
        alamatTv = (EditText) findViewById(R.id.et_alamat);
        nopeTv = (EditText) findViewById(R.id.et_telepon);
        posisi_spin = (Spinner) findViewById(R.id.spin_posisi2);
        reg_spin = (Spinner) findViewById(R.id.spin_regional2);
        prov_spin = (Spinner) findViewById(R.id.spin_provinsi2);
        kab_spin = (Spinner) findViewById(R.id.spin_kabupaten2);
        kec_spin = (Spinner) findViewById(R.id.spin_kecamatan2);
        reg_button = (Button) findViewById(R.id.btn_register);

        posisi_spin.setOnItemSelectedListener(this);
        reg_spin.setOnItemSelectedListener(this);
        prov_spin.setOnItemSelectedListener(this);
        kab_spin.setOnItemSelectedListener(this);
        kec_spin.setOnItemSelectedListener(this);

        reg_button.setOnClickListener(this);

        getPosisi();
        getRegional();
    }

    // Get Data Jabatan
    private void getPosisi() {
        ApiService service = Server.getClient().create(ApiService.class);
        Call<ItemJabatan> jabatanCall = service.ddlPosisi();
        jabatanCall.enqueue(new Callback<ItemJabatan>() {
            @Override
            public void onResponse(Call<ItemJabatan> call, Response<ItemJabatan> response) {
                if (response.isSuccessful()) {
                    posisiItem = response.body().getDdljabatan();
                    listSpinner = new ArrayList<String>();

                    for (int i = 0; i < posisiItem.size(); i++) {
                        listSpinner.add(posisiItem.get(i).getJabatan());
                    }

                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(PendaftaranActivity.this,
                            R.layout.spinner_item, listSpinner);
                    adapter.setDropDownViewResource(R.layout.spinner_item);
                    posisi_spin.setAdapter(adapter);
                } else {
                    Toast.makeText(getApplicationContext(), "Gagal mengambil data Jabatan", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ItemJabatan> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    // Get Data Regional
    private void getRegional() {
        ApiService service = Server.getClient().create(ApiService.class);
        Call<ItemRegional> call = service.ddlregional();
        call.enqueue(new Callback<ItemRegional>() {
            @Override
            public void onResponse(Call<ItemRegional> call, Response<ItemRegional> response) {
                if (response.isSuccessful()) {
                    regionalItem = response.body().getDdlregional();
                    listSpinner = new ArrayList<String>();

                    for (int i = 0; i < regionalItem.size(); i++) {
                        listSpinner.add(regionalItem.get(i).getRegional());
                    }

                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(PendaftaranActivity.this,
                            R.layout.spinner_item, listSpinner);
                    adapter.setDropDownViewResource(R.layout.spinner_item);
                    reg_spin.setAdapter(adapter);
                } else {
                    Toast.makeText(getApplicationContext(), "Gagal mengambil data Regional", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ItemRegional> call, Throwable throwable) {
                Toast.makeText(getApplicationContext(), throwable.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {
            case R.id.spin_posisi:
                kd_posisi = posisiItem.get(position).getKdjabatan();
                posisi = posisiItem.get(position).getJabatan();
                System.out.println(kd_posisi);
                break;
            case R.id.spin_regional2:
                kd_reg = regionalItem.get(position).getKode();
                regional = regionalItem.get(position).getRegional();
                getProvinsi(kd_reg);
                break;
            case R.id.spin_provinsi2:
                kd_pro = provinsiItem.get(position).getKode();
                provinsi = provinsiItem.get(position).getProvinsi();
                getKab(kd_pro);
                break;
            case R.id.spin_kabupaten2:
                kd_kab = kabupatenItem.get(position).getKode();
                kabupaten = kabupatenItem.get(position).getKabupaten();
                getKec(kd_kab);
                break;
            case R.id.spin_kecamatan2:
                kd_kec = kecamatanItem.get(position).getKdkecamatan();
                kecamatan = kecamatanItem.get(position).getKecamatan();
                System.out.println("Kd Posisi = " + kd_posisi + " Kd Regional = " + kd_reg + " Kd Pro = " + kd_pro + " Kd Kab = " + kd_kab + " Kd Kec = " + kd_kec);
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    // Get Provinsi
    private void getProvinsi(String kd_reg) {
        ApiService service = Server.getClient().create(ApiService.class);
        Call<ItemProvinsi> provinsiCall = service.dataProvinsi(kd_reg);
        provinsiCall.enqueue(new Callback<ItemProvinsi>() {
            @Override
            public void onResponse(Call<ItemProvinsi> call, Response<ItemProvinsi> response) {
                if (response.isSuccessful()) {
                    provinsiItem = response.body().getDdlprovinsi();
                    listSpinner = new ArrayList<String>();

                    for (int i = 0; i < provinsiItem.size(); i++) {
                        listSpinner.add(provinsiItem.get(i).getProvinsi());
                    }

                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(PendaftaranActivity.this,
                            R.layout.spinner_item, listSpinner);
                    adapter.setDropDownViewResource(R.layout.spinner_item);
                    prov_spin.setAdapter(adapter);
                } else {
                    Toast.makeText(getApplicationContext(), "Gagal mengambil data Provinsi", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ItemProvinsi> call, Throwable throwable) {
                Toast.makeText(getApplicationContext(), throwable.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    // Get Kabupaten
    private void getKab(String kd_pro) {
        ApiService service = Server.getClient().create(ApiService.class);
        Call<ItemKabupaten> kabupatenCall = service.getKabupaten(kd_pro);
        kabupatenCall.enqueue(new Callback<ItemKabupaten>() {
            @Override
            public void onResponse(Call<ItemKabupaten> call, Response<ItemKabupaten> response) {
                if (response.isSuccessful()) {
                    kabupatenItem = response.body().getDdlkabupaten();
                    listSpinner = new ArrayList<String>();

                    for (int i = 0; i < kabupatenItem.size(); i++) {
                        listSpinner.add(kabupatenItem.get(i).getKabupaten());
                    }

                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(PendaftaranActivity.this,
                            R.layout.spinner_item, listSpinner);
                    adapter.setDropDownViewResource(R.layout.spinner_item);
                    kab_spin.setAdapter(adapter);
                } else {
                    Toast.makeText(getApplicationContext(), "Gagal mengambil data Kabupaten", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ItemKabupaten> call, Throwable throwable) {
                Toast.makeText(getApplicationContext(), throwable.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    // Get Kecamatan
    private void getKec(String kd_kab) {
        ApiService service = Server.getClient().create(ApiService.class);
        Call<ItemKecamatan> kecamatanCall = service.getKecamatan(kd_kab);
        kecamatanCall.enqueue(new Callback<ItemKecamatan>() {
            @Override
            public void onResponse(Call<ItemKecamatan> call, Response<ItemKecamatan> response) {
                if (response.isSuccessful()) {
                    kecamatanItem = response.body().getDdldesa();
                    listSpinner = new ArrayList<String>();

                    for (int i = 0; i < kecamatanItem.size(); i++) {
                        listSpinner.add(kecamatanItem.get(i).getKecamatan());
                    }

                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(PendaftaranActivity.this,
                            R.layout.spinner_item, listSpinner);
                    adapter.setDropDownViewResource(R.layout.spinner_item);
                    kec_spin.setAdapter(adapter);
                } else {
                    Toast.makeText(getApplicationContext(), "Gagal mengambil data Kecamatan", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ItemKecamatan> call, Throwable throwable) {
                Toast.makeText(getApplicationContext(), throwable.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onClick(View v) {
        String nik = nikTv.getText().toString();
        String nama = namaTv.getText().toString();
        final String email = emailTv.getText().toString();
        String hp = nopeTv.getText().toString();

        if (nik.trim().length() < 16) {
            nikTv.setError("NIK harus 16 karakter");
            nikTv.requestFocus();
            return;
        }

        ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (conMgr.getActiveNetworkInfo() != null
                && conMgr.getActiveNetworkInfo().isAvailable()
                && conMgr.getActiveNetworkInfo().isConnected()) {

            id = "1";
            alamat = alamatTv.getText().toString();
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("nik", nik);
            editor.putString("nama", nama);
            editor.putString("email", email);
            editor.putString("hp", hp);
            editor.putString("alamat", alamat);
            editor.putString("kd_posisi", kd_posisi);
            editor.putString("posisi", posisi);
            editor.putString("kd_reg", kd_reg);
            editor.putString("regional", regional);
            editor.putString("kd_pro", kd_pro);
            editor.putString("provinsi", provinsi);
            editor.putString("kd_kab", kd_kab);
            editor.putString("kabupaten", kabupaten);
            editor.putString("kd_kec", kd_kec);
            editor.putString("kecamatan", kecamatan);
            editor.commit();

            db = new SQLiteHelper(getApplicationContext());
            //db.insertDataPribadi(new DataPribadiModel(id, nama, email, hp, alamat, kd_posisi, kd_pro, provinsi, kd_kab, kabupaten, kd_kec, kecamatan));

            //getRegister(nik, nama, email, hp, kd_posisi);
        } else {
            AlertDialog alertDialog = new AlertDialog.Builder(this).create();
            alertDialog.setTitle("Connection Error!");
            alertDialog.setMessage("Pastikan anda terhubung dengan koneksi internet, aktifkan Paket data atau Wifi");
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();
        }

    }

    // submit register
    private void getRegister(final String nik, final String nama, final String email, final String hp, final String kd_posisi) {
        progressDialog.setTitle("Proses Pendaftaran ...");
        progressDialog.setMessage("Tunggu Sebentar ...");
        progressDialog.show();

        ApiService service = Server.getClient().create(ApiService.class);
        Call<ItemRegister> registerCall = service.getRegister(nik, nama, hp, email, kd_posisi);
        registerCall.enqueue(new Callback<ItemRegister>() {
            @Override
            public void onResponse(Call<ItemRegister> call, Response<ItemRegister> response) {
                ItemRegister model = response.body();

                try {
                    if (model.getSuccess()) {
                        Toast.makeText(getBaseContext(), model.getMsg(), Toast.LENGTH_SHORT).show();

                        AlertDialog alertDialog = new AlertDialog.Builder(getApplicationContext()).create();
                        alertDialog.setTitle("Connection Error!");
                        alertDialog.setMessage(model.getMsg());
                        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        alertDialog.show();
                        progressDialog.dismiss();
                    } else {
                        Toast.makeText(getBaseContext(), model.getMsg(), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }

            }

            @Override
            public void onFailure(Call<ItemRegister> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                if (progressDialog != null && progressDialog.isShowing()){
                    progressDialog.dismiss();
                }
            }
        });
    }
}
