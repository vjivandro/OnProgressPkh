package com.sdmpkh.kemensos.Service;

import com.sdmpkh.kemensos.model.ItemJabatan;
import com.sdmpkh.kemensos.model.ItemKabupaten;
import com.sdmpkh.kemensos.model.ItemKecamatan;
import com.sdmpkh.kemensos.model.ItemProvinsi;
import com.sdmpkh.kemensos.model.ItemRegional;
import com.sdmpkh.kemensos.model.ItemRegister;
import com.sdmpkh.kemensos.model.RegionalModel;
import com.sdmpkh.kemensos.model.RegisterModel;

import org.json.JSONObject;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by ivandro on 6/25/18.
 */

public interface ApiService {
    @FormUrlEncoded
    @POST("pendaftaran")
    Call<ItemRegister> getRegister(@Field("nik") String nik,
                                   @Field("nama") String nama,
                                   @Field("nohp") String ponsel,
                                   @Field("email") String email,
                                   @Field("posisi") String posisi);

    @GET("ddljabatan")
    Call<ItemJabatan> ddlPosisi();

//    @GET("/formasi.json")
//    Call<JSONObject> ddlPosisi();

    @GET("ddlregional")
    Call<ItemRegional> ddlregional();

    @FormUrlEncoded
    @POST("ddlprovinsi")
    Call<ItemProvinsi> dataProvinsi(@Field("regional") String regional);

    @FormUrlEncoded
    @POST("ddlkabupaten")
    Call<ItemKabupaten> getKabupaten(@Field("provinsi") String provinsi);

    @FormUrlEncoded
    @POST("ddlkecamatan")
    Call<ItemKecamatan> getKecamatan(@Field("kabupaten") String kabupaten);



}
