package com.sdmpkh.kemensos.activity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.sdmpkh.kemensos.R;
import com.sdmpkh.kemensos.helper.SQLiteHelper;
import com.sdmpkh.kemensos.model.DataPribadiModel;
import com.sdmpkh.kemensos.model.PenghargaanModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class Penghargaan extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener, AdapterView.OnItemSelectedListener {

    EditText nama_penghargaan_tv, keterangan_tv;
    Spinner spinner_th;
    Button submit_btn;

    private SQLiteHelper db;

    private Calendar tanggalLahir;
    private DatePickerDialog.OnDateSetListener date;
    private SimpleDateFormat simpleDateFormat;

    String th_penghargaan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_penghargaan);

        db = new SQLiteHelper(this);

        nama_penghargaan_tv = (EditText) findViewById(R.id.et_namapenghargaan);

        keterangan_tv = (EditText) findViewById(R.id.et_keterangan);
        submit_btn = (Button) findViewById(R.id.btn_simpan);

        ArrayList<String> years = new ArrayList<String>();
        int thisYear = Calendar.getInstance().get(Calendar.YEAR);
        for (int i = 1990; i <= thisYear; i++) {
            years.add(Integer.toString(i));
        }
        ArrayAdapter<String> adapters = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, years);
        spinner_th = (Spinner) findViewById(R.id.spin_tahunpenghargaan);
        spinner_th.setAdapter(adapters);

        spinner_th.setOnItemSelectedListener(this);

        submit_btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        String id = "";
        String penghargaan = nama_penghargaan_tv.getText().toString();
        String keterangan = keterangan_tv.getText().toString();

        simpanPenghargaan(penghargaan, th_penghargaan, keterangan);
    }

    private void simpanPenghargaan(String penghargaan, String tahun, String keterangan) {

        if (nama_penghargaan_tv.getText().toString().trim().equals("") || keterangan_tv.getText().toString().trim().equals("") ) {
            Toast.makeText(getApplicationContext(), "Maaf tidak boleh kosong", Toast.LENGTH_LONG).show();
        } else {
            db.insertPenghargaan(new PenghargaanModel(penghargaan, tahun, keterangan));

            AlertDialog alertDialog = new AlertDialog.Builder(this).create();
            alertDialog.setTitle("Pemberitahuan");
            alertDialog.setMessage("Menyimpan Data Berhasil");
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Lanjutkan",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            getBlank();
                            finish();
                        }
                    });
            alertDialog.show();
        }
    }

    private void getBlank() {
        nama_penghargaan_tv.setText(null);
        keterangan_tv.setText(null);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        th_penghargaan = parent.getItemAtPosition(position).toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
