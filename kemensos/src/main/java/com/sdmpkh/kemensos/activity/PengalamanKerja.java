package com.sdmpkh.kemensos.activity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.sdmpkh.kemensos.R;
import com.sdmpkh.kemensos.helper.SQLiteHelper;
import com.sdmpkh.kemensos.model.PengalamanModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class PengalamanKerja extends AppCompatActivity implements AdapterView.OnItemSelectedListener, View.OnClickListener {

    private EditText jenispengalaman_et, keterangan_et;
    private Button submit_btn;
    private SQLiteHelper db;
    private String th_lulus, th_masuk;
    private Calendar tanggalLahir;
    private DatePickerDialog.OnDateSetListener date;
    private SimpleDateFormat simpleDateFormat;
    private Spinner spin_keluar_kerja, spin_masuk_kerja;
    int lulus, masuk;
    List<String> listSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pengalaman_kerja);

        db = new SQLiteHelper(this);

        jenispengalaman_et = (EditText) findViewById(R.id.et_jenispekerjaan);
        keterangan_et = (EditText) findViewById(R.id.et_keterangan);
        submit_btn = (Button) findViewById(R.id.btn_simpan);

        ArrayList<String> years = new ArrayList<String>();
        int thisYear = Calendar.getInstance().get(Calendar.YEAR);
        for (int i = 1990; i <= thisYear; i++) {
            years.add(Integer.toString(i));
        }
        ArrayAdapter<String> adapters = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, years);

        spin_keluar_kerja = (Spinner)findViewById(R.id.spin_thlulus);
        spin_keluar_kerja.setAdapter(adapters);


        spin_masuk_kerja = (Spinner) findViewById(R.id.spin_thmasuk);
        ArrayList<String> thmasuk = new ArrayList<String>();
        masuk = Calendar.getInstance().get(Calendar.YEAR);
        for (int m = 1990; m <= masuk; m++) {
            thmasuk.add(Integer.toString(m));
        }
        ArrayAdapter<String> adapter_masuk = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, thmasuk);
        spin_masuk_kerja.setAdapter(adapter_masuk);


        spin_keluar_kerja.setOnItemSelectedListener(this);
        spin_masuk_kerja.setOnItemSelectedListener(this);

        submit_btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        String id = "";
        String jenispengalaman = jenispengalaman_et.getText().toString();
        String keterangan = keterangan_et.getText().toString();

        SimpanPengalaman(jenispengalaman, th_lulus  , th_masuk, keterangan);
        //System.out.println(jenispengalaman + tahunkeluar + tahunmasuk + keterangan_tv);
    }

    private void SimpanPengalaman(String jenispengalaman, String tahunkeluar, String tahunmasuk, String keterangan) {
        if (jenispengalaman_et.getText().toString().trim().equals("") || keterangan_et.getText().toString().trim().equals("")) {

            Toast.makeText(getApplicationContext(), "Maaf tidak boleh kosong", Toast.LENGTH_LONG).show();
        } else {
            db.insertPengalaman(new PengalamanModel(jenispengalaman, tahunkeluar, tahunmasuk, keterangan));
            AlertDialog alertDialog = new AlertDialog.Builder(this).create();
            alertDialog.setTitle("Pemberitahuan");
            alertDialog.setMessage("Menyimpan Data Berhasil");
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Lanjutkan",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            getBlank();
                            finish();
                        }
                    });
            alertDialog.show();
        }
    }

    private void getBlank() {
        jenispengalaman_et.setText(null);
        keterangan_et.setText(null);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {
            case R.id.spin_thlulus:
                th_lulus = parent.getItemAtPosition(position).toString();
                System.out.println(th_lulus);
                break;
            case R.id.spin_thmasuk:
                th_masuk = parent.getItemAtPosition(position).toString();
                System.out.println(th_masuk);
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
