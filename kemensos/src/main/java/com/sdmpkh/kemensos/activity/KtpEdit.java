package com.sdmpkh.kemensos.activity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.sdmpkh.kemensos.R;
import com.sdmpkh.kemensos.helper.SQLiteHelper;
import com.sdmpkh.kemensos.model.DataKtpModel;
import com.sdmpkh.kemensos.model.DataPribadiModel;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class KtpEdit extends AppCompatActivity implements RadioGroup.OnCheckedChangeListener, View.OnClickListener {

    private  TextView id_tv, resultRbTv, ktp_label, ijasah_label, domisili_label; // Edit Data Pribadi
    private EditText nik_etktp, tmptLahir_etktp, tglLahir_etktp, kebangsaan_etktp, status_etktp, propinsi_etktp,
            kabupaten_etktp, kecamatan_etktp; // Edit Data KTP
    private Spinner spin_kebangsaan, spin_status;
    private Button submit_btn;

    private String dateFormat, nik, kdjk, jk, kebangsaan, status, provinsi, kabupaten, kecamatan;

    private Calendar tanggalLahir;
    private DatePickerDialog.OnDateSetListener date;
    private SimpleDateFormat simpleDateFormat;
    private RadioGroup radioGroup;
    private RadioButton maleRb, femaleRb;

    private SharedPreferences sharedPreferences;
    private SQLiteHelper db;

    public static final String my_shared_preferences = "my_shared_preferences";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_dataktp);

        db = new SQLiteHelper(KtpEdit.this);

        sharedPreferences = getSharedPreferences(my_shared_preferences, Context.MODE_PRIVATE);
        nik = sharedPreferences.getString("nik", "");
        provinsi = sharedPreferences.getString("provinsi", "");
        kabupaten = sharedPreferences.getString("kabupaten", "");
        kecamatan = sharedPreferences.getString("kecamatan", "");

        id_tv = (TextView) findViewById(R.id.id);
        nik_etktp = (EditText) findViewById(R.id.et_nik);
        nik_etktp.setText(nik);
        tmptLahir_etktp = (EditText) findViewById(R.id.et_tmptLahir);
        tglLahir_etktp = (EditText) findViewById(R.id.et_tglLahir);
        propinsi_etktp = (EditText) findViewById(R.id.et_propinsi);
        kabupaten_etktp = (EditText) findViewById(R.id.et_kabupaten);
        kecamatan_etktp = (EditText) findViewById(R.id.et_kecamatan);
        radioGroup = (RadioGroup) findViewById(R.id.rg_sex);
        maleRb = (RadioButton) findViewById(R.id.rb_male);
        femaleRb = (RadioButton) findViewById(R.id.rb_female);
        resultRbTv = (TextView) findViewById(R.id.tv_resultRb);
        resultRbTv = (TextView) findViewById(R.id.tv_resultRb);
        submit_btn = (Button) findViewById(R.id.btn_simpan);

        propinsi_etktp.setText(provinsi);
        kabupaten_etktp.setText(kabupaten);
        kecamatan_etktp.setText(kecamatan);

        spin_kebangsaan = (Spinner) findViewById(R.id.spin_kebangsaan);
        ArrayAdapter<CharSequence> adapterKebangsaan = ArrayAdapter.createFromResource(this,
                R.array.warganegara, android.R.layout.simple_spinner_item);
        adapterKebangsaan.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin_kebangsaan.setAdapter(adapterKebangsaan);

        spin_status = (Spinner) findViewById(R.id.spin_status);
        ArrayAdapter<CharSequence> adapterStatus = ArrayAdapter.createFromResource(this,
                R.array.status, android.R.layout.simple_spinner_item);
        adapterStatus.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin_status.setAdapter(adapterStatus);

        spin_kebangsaan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                kebangsaan = String.valueOf(spin_kebangsaan.getItemAtPosition(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spin_status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                status = String.valueOf(spin_status.getItemAtPosition(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        tglLahir_etktp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDate();
            }
        });

        radioGroup.setOnCheckedChangeListener(this);
        submit_btn.setOnClickListener(this);

        tanggalLahir = Calendar.getInstance();
        date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                tanggalLahir.set(Calendar.YEAR, year);
                tanggalLahir.set(Calendar.MONTH, monthOfYear);
                tanggalLahir.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }
        };
    }

    private void getDate() {
        new DatePickerDialog(KtpEdit.this, date,
                tanggalLahir.get(Calendar.YEAR),
                tanggalLahir.get(Calendar.MONTH),
                tanggalLahir.get(Calendar.DAY_OF_MONTH)).show();
    }


    private void updateLabel() {
        dateFormat = "dd/MM/yy";
        simpleDateFormat = new SimpleDateFormat(dateFormat, Locale.US);
        tglLahir_etktp.setText(simpleDateFormat.format(tanggalLahir.getTime()));
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        if (maleRb.isChecked()) {
            resultRbTv.setText("1");
            kdjk = "1";
            jk = "Laki - Laki";
        } else {
            resultRbTv.setText("2");
            kdjk = "2";
            jk = "Perempuan";
        }
    }

    // menyimpan data ktp
    private void simpanDataKTP() {
        String id = "1";
        String nik = nik_etktp.getText().toString();
        String tmpLahir = tmptLahir_etktp.getText().toString();
        String tanggalLahir = tglLahir_etktp.getText().toString();
        String prov = propinsi_etktp.getText().toString();
        String kab = kabupaten_etktp.getText().toString();
        String kec = kecamatan_etktp.getText().toString();

        if (nik_etktp.getText().toString().trim().equals("") ||
                tmptLahir_etktp.getText().toString().trim().equals("") ||
                tglLahir_etktp.getText().toString().trim().equals("")){
            Toast.makeText(getApplicationContext(), "Maaf tidak boleh kosong", Toast.LENGTH_LONG).show();
        } else {
            db.insertDataKtp(new DataKtpModel(id, nik, tmpLahir, tanggalLahir, kdjk, jk, kebangsaan, status, prov, kab, kec));
            AlertDialog alertDialog = new AlertDialog.Builder(this).create();
            alertDialog.setTitle("Pemberitahuan");
            alertDialog.setMessage("Menyimpan Data Berhasil");
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Lanjutkan",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            getBlank();
                            finish();
                        }
                    });
            alertDialog.show();
        }
    }

    private void getBlank() {
        nik_etktp.setText(null);
        tmptLahir_etktp.setText(null);
        tglLahir_etktp.setText(null);
        propinsi_etktp.setText(null);
        kabupaten_etktp.setText(null);
        kecamatan_etktp.setText(null);
    }

    @Override
    public void onClick(View v) {
        simpanDataKTP();
    }

}
