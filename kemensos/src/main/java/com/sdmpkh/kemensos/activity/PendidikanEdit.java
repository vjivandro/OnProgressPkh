package com.sdmpkh.kemensos.activity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.sdmpkh.kemensos.R;
import com.sdmpkh.kemensos.helper.SQLiteHelper;
import com.sdmpkh.kemensos.model.PendidikanModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class PendidikanEdit extends AppCompatActivity implements AdapterView.OnItemSelectedListener, View.OnClickListener {

    private EditText sekolah_et, penjrurusan_et, thlulus_et, thmasuk_et, ipk_et;
    private Button submit_btn;
    private TextView id_tv, simpanBtn;
    private SQLiteHelper db;
    private Spinner pendidikan_spin, spin_lulus, spin_masuk;

    private Calendar tanggalLahir;
    private DatePickerDialog.OnDateSetListener date;
    private SimpleDateFormat simpleDateFormat;

    private String kdtingkat, tingkat, th_lulus, th_masuk;
    List<String> listSpinner;
    int lulus, masuk;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_pendidikan);
        db = new SQLiteHelper(this);

        sekolah_et = (EditText) findViewById(R.id.et_sekolah);
        penjrurusan_et = (EditText) findViewById(R.id.et_penjurusan);
        ipk_et = (EditText) findViewById(R.id.et_ipk);
        submit_btn = (Button) findViewById(R.id.btn_simpan);

        pendidikan_spin = (Spinner) findViewById(R.id.pend_spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.pendidikan_terakhir, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        pendidikan_spin.setAdapter(adapter);

        ArrayList<String> years = new ArrayList<String>();
        int thisYear = Calendar.getInstance().get(Calendar.YEAR);
        for (int i = 1990; i <= thisYear; i++) {
            years.add(Integer.toString(i));
        }
        ArrayAdapter<String> adapters = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, years);

        spin_lulus = (Spinner)findViewById(R.id.spin_thlulus);
        spin_lulus.setAdapter(adapters);


        spin_masuk = (Spinner) findViewById(R.id.spin_thmasuk);
        ArrayList<String> thmasuk = new ArrayList<String>();
        masuk = Calendar.getInstance().get(Calendar.YEAR);
        for (int m = 1990; m <= masuk; m++) {
            thmasuk.add(Integer.toString(m));
        }
        ArrayAdapter<String> adapter_masuk = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, thmasuk);
        spin_masuk.setAdapter(adapter_masuk);

        pendidikan_spin.setOnItemSelectedListener(this);
        spin_lulus.setOnItemSelectedListener(this);
        spin_masuk.setOnItemSelectedListener(this);

        submit_btn.setOnClickListener(this);

    }

    private void simpanDataPendidikan() {

        String sekolah = sekolah_et.getText().toString();
        String penjurusan = penjrurusan_et.getText().toString();
        String ipk = ipk_et.getText().toString();

        simpanData(sekolah, tingkat, kdtingkat, penjurusan, th_lulus  , th_masuk, ipk);
//        System.out.println("sekolah = "+sekolah +"tingkat = "+ tingkat +" kdtingkat = " + kdtingkat + "penjurusan =" +penjurusan+ "ipk = " +ipk);
    }

    private void simpanData(String sekolah, String kdtingkat, String tingkat, String penjurusan, String thlulus, String thmasuk, String ipk) {
        if (sekolah_et.getText().toString().trim().equals("") || penjrurusan_et.getText().toString().trim().equals("")) {

            Toast.makeText(getApplicationContext(), "Maaf tidak boleh kosong", Toast.LENGTH_LONG).show();
        } else {
            db.insertPendidikan(new PendidikanModel(kdtingkat, tingkat, sekolah, penjurusan, thlulus, thmasuk, ipk));

            //values.put(COLUMN_KDTINGKAT, pendidikanModel.getKdtingkat());
            //        values.put(COLUMN_TINGKAT, pendidikanModel.getTingkat());
            //        values.put(COLUMN_SEKOLAH, pendidikanModel.getSekolah());
            //        values.put(COLUMN_PENJURUSAN, pendidikanModel.getPenjurusan());
            //        values.put(COLUMN_LULUS, pendidikanModel.getThlulus());
            //        values.put(COLUMN_MASUK, pendidikanModel.getThmasuk());
            //        values.put(COLUMN_IPK, pendidikanModel.getIpk());

            AlertDialog alertDialog = new AlertDialog.Builder(this).create();
            alertDialog.setTitle("Pemberitahuan");
            alertDialog.setMessage("Menyimpan Data Berhasil");
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Lanjutkan",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            getBlank();
                            finish();
                        }
                    });
            alertDialog.show();
        }
    }

    private void getBlank() {
        sekolah_et.setText(null);
        penjrurusan_et.setText(null);
        sekolah_et.setText(null);
        ipk_et.setText(null);
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        kdtingkat = String.valueOf(pendidikan_spin.getItemIdAtPosition(position));
        switch (parent.getId()) {
            case R.id.pend_spinner:
                kdtingkat = parent.getItemAtPosition(position).toString();
                tingkat = parent.getItemAtPosition(position).toString();
                break;
            case R.id.spin_thlulus:
                th_lulus = parent.getItemAtPosition(position).toString();
                System.out.println(th_lulus);
                break;
            case R.id.spin_thmasuk:
                th_masuk = parent.getItemAtPosition(position).toString();
                System.out.println(th_masuk);
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case  R.id.btn_simpan:
                simpanDataPendidikan();
                break;
        }
    }
}
