package com.sdmpkh.kemensos.activity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.sdmpkh.kemensos.R;
import com.sdmpkh.kemensos.helper.SQLiteHelper;
import com.sdmpkh.kemensos.model.DataPribadiModel;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import pl.aprilapps.easyphotopicker.EasyImage;

public class EditDataDiriActivity extends AppCompatActivity implements View.OnClickListener, EasyImage.Callbacks {

    private Bitmap bpKtp, bpIjasah, bpDomisili;
    private Button sumbit_btn;
    private ImageView ktp_img, ijasah_img, domisisli_img;
    private EditText id_et, nama_et, email_et, ponsel_et, alamat_et, propinsi_et, kabupaten_et, kecamatan_et; // Edit Data Pribadi
    private TextView pathKtp_tv, pathIjasah_tv, pathDomisili_tv;
    private byte[] foto_ktp, foto_ijasah, foto_domisili;

    private String id, nik, nama, email, ponsel, alamat, kd_posisi, posisi, kd_pro, provinsi, kd_kab, kabupaten, kd_kec, kecamatan, ft_ktp, ft_ijasah, ft_domisili;

    private SharedPreferences sharedPreferences;
    private Calendar tanggalLahir;
    private DatePickerDialog.OnDateSetListener date;
    private SimpleDateFormat simpleDateFormat;
    private SQLiteHelper db;

    public static final int REQUEST_CODE_CAMERA = 0012;
    public static final int REQUEST_CODE_GALLERY = 0013;
    private String encodeImageKtp, encodeImageIjasah, encodeImageDomisili;
    String pathKtp, pathIjasah, pathDomisili;
    private String [] items = {"Camera","Gallery"};

    public static final String my_shared_preferences = "my_shared_preferences";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_data_diri);

        sharedPreferences = getSharedPreferences(my_shared_preferences, Context.MODE_PRIVATE);
        db = new SQLiteHelper(this);

        nik = sharedPreferences.getString("nik", "");

        sharedPreferences = getSharedPreferences(my_shared_preferences, Context.MODE_PRIVATE);
        nik = sharedPreferences.getString("nik", "");
        nama = sharedPreferences.getString("nama", "");
        email = sharedPreferences.getString("email", "");
        ponsel = sharedPreferences.getString("hp", "");
        alamat = sharedPreferences.getString("alamat", "");
        kd_posisi = sharedPreferences.getString("kd_posisi", "");
        posisi = sharedPreferences.getString("posisi", "");
        kd_pro = sharedPreferences.getString("kd_pro", "");
        provinsi = sharedPreferences.getString("provinsi", "");
        kd_kab = sharedPreferences.getString("kd_kab", "");
        kabupaten = sharedPreferences.getString("kabupaten", "");
        kd_kec = sharedPreferences.getString("kd_kec", "");
        kecamatan = sharedPreferences.getString("kecamatan", "");

        id_et = (EditText) findViewById(R.id.id);
        id_et.setText("1");
        nama_et = (EditText) findViewById(R.id.et_nama);
        email_et = (EditText) findViewById(R.id.et_email);
        ponsel_et = (EditText) findViewById(R.id.et_ponsel);
        alamat_et = (EditText) findViewById(R.id.et_alamat);
        propinsi_et = (EditText) findViewById(R.id.et_propinsi);
        kabupaten_et = (EditText) findViewById(R.id.et_kabupaten);
        kecamatan_et = (EditText) findViewById(R.id.et_kecamatan);
        ktp_img = (ImageView) findViewById(R.id.image_ktp);
        ijasah_img = (ImageView) findViewById(R.id.image_ijasah);
        domisisli_img = (ImageView) findViewById(R.id.image_domisili);
        sumbit_btn = (Button) findViewById(R.id.btn_simpan);

//        DataPribadiModel dataPribadiModel = db.getAllDataPribadi();
        nama_et.setText(nama);
        email_et.setText(email);
        alamat_et.setText(alamat);
        ponsel_et.setText(ponsel);
        propinsi_et.setText(provinsi);
        kabupaten_et.setText(kabupaten);
        kecamatan_et.setText(kecamatan);

        ktp_img.setOnClickListener(this);
        ijasah_img.setOnClickListener(this);
        domisisli_img.setOnClickListener(this);

        sumbit_btn.setOnClickListener(this);

    }


    private void simpanDataPribadi() {
        id = id_et.getText().toString();
        nama = nama_et.getText().toString();
        email = email_et.getText().toString();
        ponsel = ponsel_et.getText().toString();
        alamat = alamat_et.getText().toString();
        provinsi = propinsi_et.getText().toString();
        kabupaten = kabupaten_et.getText().toString();
        kecamatan = kecamatan_et.getText().toString();

//        if (id_et.getText().toString().trim().equals("") || nama_et.getText().toString().trim().equals("") ||
//                email_et.getText().toString().trim().equals("") || ponsel_et.getText().toString().equals("") ||
//                alamat_et.getText().toString().trim().equals("") || propinsi_et.getText().toString().equals("") ||
//                kabupaten_et.getText().toString().equals("") || kecamatan_et.getText().toString().equals("")) {
//            Toast.makeText(getApplicationContext(), "Maaf tidak boleh kosong", Toast.LENGTH_LONG).show();
//        } else {
//            String kd_posisi = "400";
//            String kd_pro = "1";
//            String kd_kab = "11";
//            String kd_kec = "111";
//            db.insertDataPribadi(new DataPribadiModel(id, nama, email, ponsel, alamat, kd_posisi, kd_pro, propinsi, kd_kab,kabupaten, kd_kec,kecamatan));
//
//        }

        db.updateDataPribadi(new DataPribadiModel(nama, email, ponsel, alamat));
        System.out.println(nama + " " + email + " " + ponsel + " " + alamat);

//            db.update(id_et.getText().toString().trim(), nama_et.getText().toString().trim(), email_et.getText().toString().trim(),
//                    ponsel_et.getText().toString(), alamat_et.getText().toString().trim());

        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Pemberitahuan");
        alertDialog.setMessage("Menyimpan Data Berhasil");
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Lanjutkan",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        getBlank();
                        finish();
                    }
                });
        alertDialog.show();
    }

    private void getBlank() {
        id_et.setText(null);
        nama_et.setText(null);
        email_et.setText(null);
        ponsel_et.setText(null);
        alamat_et.setText(null);
        propinsi_et.setText(null);
        kabupaten_et.setText(null);
        kecamatan_et.setText(null);
    }

    @Override
    public void onClick(View v) {
        int id  = v.getId();

        switch (id) {
            case R.id.btn_simpan:
                simpanDataPribadi();
                String ktpblob = sharedPreferences.getString("blobKtp", "");
                String ijasahblob = sharedPreferences.getString("blobIjasah", "");
                String domisiliblob = sharedPreferences.getString("blobDomisili", "");

                System.out.println("KTP => " + ktpblob + "\n Ijasah => " + ijasahblob + "\n Domisili => " + domisiliblob);
                break;
            case R.id.image_ktp:
                openImageKtp();
                break;
            case R.id.image_ijasah:
                openImageIjasah();
                break;
            case R.id.image_domisili:
                openImageDomisili();
                break;

        }
    }

    private void openImageKtp() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Options");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if(items[i].equals("Camera")){
                    EasyImage.openCamera(EditDataDiriActivity.this,0);

                }else if(items[i].equals("Gallery")){
                    EasyImage.openGallery(EditDataDiriActivity.this, 10);
                }
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void openImageIjasah() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Options");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if(items[i].equals("Camera")){
                    EasyImage.openCamera(EditDataDiriActivity.this,1);

                }else if(items[i].equals("Gallery")){
                    EasyImage.openGallery(EditDataDiriActivity.this, 11);
                }
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void openImageDomisili() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Options");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if(items[i].equals("Camera")){
                    EasyImage.openCamera(EditDataDiriActivity.this,2);

                }else if(items[i].equals("Gallery")){
                    EasyImage.openGallery(EditDataDiriActivity.this, 12);
                }
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        EasyImage.handleActivityResult(requestCode, resultCode, data, this, this);
    }

    @Override
    public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {

    }

    @Override
    public void onImagePicked(File imageFile, EasyImage.ImageSource source, int type) {

        switch (type) {
            case 0:
                Glide.with(EditDataDiriActivity.this)
                        .load(imageFile)
                        .centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.ALL);

                // Convert image to base64
//                Bitmap bm = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
//                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//                bm.compress(Bitmap.CompressFormat.JPEG, 75, byteArrayOutputStream);
//                String cameraEncode = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
//
//                encodeImageKtp = cameraEncode.replaceAll("%", "");

                pathKtp = imageFile.getAbsolutePath();

                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("blobKtp", pathKtp);
                editor.commit();

                System.out.println("Camera => " + imageFile.getAbsolutePath());
                break;

            case 1:
                Glide.with(EditDataDiriActivity.this)
                        .load(imageFile)
                        .centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.ALL);

                // Convert image to base64
//                Bitmap bmijasah = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
//                ByteArrayOutputStream baosIjasah = new ByteArrayOutputStream();
//                bmijasah.compress(Bitmap.CompressFormat.JPEG, 75, baosIjasah);
//                String cameraEncodesIjasah = Base64.encodeToString(baosIjasah.toByteArray(), Base64.DEFAULT);
//
//                encodeImageIjasah = cameraEncodesIjasah.replaceAll("%", "");
//
                pathIjasah = imageFile.getAbsolutePath();
                SharedPreferences.Editor edit = sharedPreferences.edit();
                edit.putString("blobIjasah", pathIjasah);
                edit.commit();

                System.out.println("Camera => " + imageFile.getAbsolutePath());
                break;
            case 2:
                Glide.with(EditDataDiriActivity.this)
                        .load(imageFile)
                        .diskCacheStrategy(DiskCacheStrategy.ALL);


                // Convert image to base64
//                Bitmap bmDomisili = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
//                ByteArrayOutputStream baosDomisili = new ByteArrayOutputStream();
//                bmDomisili.compress(Bitmap.CompressFormat.JPEG, 75, baosDomisili);
//                String cameraEncodesDomisili = Base64.encodeToString(baosDomisili.toByteArray(), Base64.DEFAULT);
//
//                encodeImageDomisili = cameraEncodesDomisili.replaceAll("%", "");

                pathDomisili = imageFile.getAbsolutePath();

                SharedPreferences.Editor edito = sharedPreferences.edit();
                edito.putString("blobDomisili", pathDomisili);
                edito.commit();

                System.out.println("Camera => " + imageFile.getAbsolutePath());
                break;

            case 10:
                Glide.with(EditDataDiriActivity.this)
                        .load(imageFile)
                        .diskCacheStrategy(DiskCacheStrategy.ALL);


                // Convert image to base64
//                Bitmap bmDomisili = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
//                ByteArrayOutputStream baosDomisili = new ByteArrayOutputStream();
//                bmDomisili.compress(Bitmap.CompressFormat.JPEG, 75, baosDomisili);
//                String cameraEncodesDomisili = Base64.encodeToString(baosDomisili.toByteArray(), Base64.DEFAULT);
//
//                encodeImageDomisili = cameraEncodesDomisili.replaceAll("%", "");

                pathKtp = imageFile.getAbsolutePath();

                SharedPreferences.Editor editogal = sharedPreferences.edit();
                editogal.putString("blobKtp", pathKtp);
                editogal.commit();

                System.out.println("Camera => " + imageFile.getAbsolutePath());
                break;
            case 11:
                Glide.with(EditDataDiriActivity.this)
                        .load(imageFile)
                        .diskCacheStrategy(DiskCacheStrategy.ALL);


                // Convert image to base64
//                Bitmap bmDomisili = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
//                ByteArrayOutputStream baosDomisili = new ByteArrayOutputStream();
//                bmDomisili.compress(Bitmap.CompressFormat.JPEG, 75, baosDomisili);
//                String cameraEncodesDomisili = Base64.encodeToString(baosDomisili.toByteArray(), Base64.DEFAULT);
//
//                encodeImageDomisili = cameraEncodesDomisili.replaceAll("%", "");

                pathIjasah = imageFile.getAbsolutePath();

                SharedPreferences.Editor editogalIjasah = sharedPreferences.edit();
                editogalIjasah.putString("blobIjasah", pathIjasah);
                editogalIjasah.commit();

                System.out.println("Camera => " + imageFile.getAbsolutePath());
                break;
            case 12:
                Glide.with(EditDataDiriActivity.this)
                        .load(imageFile)
                        .diskCacheStrategy(DiskCacheStrategy.ALL);


                // Convert image to base64
//                Bitmap bmDomisili = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
//                ByteArrayOutputStream baosDomisili = new ByteArrayOutputStream();
//                bmDomisili.compress(Bitmap.CompressFormat.JPEG, 75, baosDomisili);
//                String cameraEncodesDomisili = Base64.encodeToString(baosDomisili.toByteArray(), Base64.DEFAULT);
//
//                encodeImageDomisili = cameraEncodesDomisili.replaceAll("%", "");

                pathDomisili = imageFile.getAbsolutePath();

                SharedPreferences.Editor editoDomGal = sharedPreferences.edit();
                editoDomGal.putString("blobDomisili", pathDomisili);
                editoDomGal.commit();

                System.out.println("Camera => " + imageFile.getAbsolutePath());
                break;
        }

    }

    @Override
    public void onCanceled(EasyImage.ImageSource source, int type) {

    }

    @Override
    protected void onResume() {
        super.onResume();

        sharedPreferences = getSharedPreferences(my_shared_preferences, Context.MODE_PRIVATE);
        pathKtp = sharedPreferences.getString("blobKtp", "");
        pathIjasah = sharedPreferences.getString("blobIjasah", "");
        pathDomisili = sharedPreferences.getString("blobDomisili", "");

        if (pathKtp.equals("")) {
            ktp_img.setImageResource(R.drawable.ic_image);
        } else {
            File file = new File(pathKtp);
            if (file.exists()) {
                Bitmap bp = BitmapFactory.decodeFile(file.getAbsolutePath());
                ktp_img.setImageBitmap(bp);
            }
        }

        if (pathIjasah.equals("")) {
            ijasah_img.setImageResource(R.drawable.ic_image);
        } else {
            File file = new File(pathIjasah);
            if (file.exists()) {
                Bitmap bp = BitmapFactory.decodeFile(file.getAbsolutePath());
                ijasah_img.setImageBitmap(bp);
            }
        }

        if (pathDomisili.equals("")) {
            domisisli_img.setImageResource(R.drawable.ic_image);
        } else {
            File file = new File(pathDomisili);
            if (file.exists()) {
                Bitmap bp = BitmapFactory.decodeFile(file.getAbsolutePath());
                domisisli_img.setImageBitmap(bp);
            }
        }
    }
}
