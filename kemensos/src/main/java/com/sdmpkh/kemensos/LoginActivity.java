package com.sdmpkh.kemensos;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telecom.Call;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.sdmpkh.kemensos.App.AppController;
import com.sdmpkh.kemensos.Service.ApiService;
import com.sdmpkh.kemensos.utils.Server;
import com.sdmpkh.kemensos.utils.SharedPrefManager;
import com.xwray.passwordview.PasswordView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private String url = Server.LOGIN_URL;
    private ProgressDialog progressDialog;
    private EditText username_et;
    private PasswordView password_et;
    private TextInputLayout passwordViewTIL;
    private TextView registerTv;
    private Intent intent;
    private Button btn_login;
    private ConnectivityManager conMgr;
    SharedPreferences sharedPreferences;
    boolean success;
    boolean session = false;

    String username, password;
    String nama, email, ponsel, alamat, kd_posisi, posisi, kd_pro, provinsi, kd_kab, kabupaten, kd_kec, kecamatan;
    String tag_jObj_obj = "json_obj_req";
    String nik, idpersonal;

    public static final String my_shared_preferences = "my_shared_preferences";
    public static final String session_status = "session_status";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        {
            if (conMgr.getActiveNetworkInfo() != null
                    && conMgr.getActiveNetworkInfo().isAvailable()
                    && conMgr.getActiveNetworkInfo().isConnected()) {

            } else {
                Toast.makeText(getApplicationContext(), "Periksa koneksi internet",
                        Toast.LENGTH_LONG).show();
            }
        }

        progressDialog = new ProgressDialog(this);

        btn_login = (Button) findViewById(R.id.btn_login);
        username_et = (EditText) findViewById(R.id.tv_username);
        password_et = (PasswordView) findViewById(R.id.password);
        passwordViewTIL = (TextInputLayout) findViewById(R.id.password_strike_til);
        registerTv = (TextView) findViewById(R.id.tv_register);

        username = username_et.getText().toString();
        password = password_et.getText().toString();

        username_et.setText("3509182111900001");
        password_et.setText("pakarifganteng");

        // cek session login jika TRUE langsung menjalankan MainActivity.class
        sharedPreferences = getSharedPreferences(my_shared_preferences, Context.MODE_PRIVATE);
        session = sharedPreferences.getBoolean(session_status, false);
        nik = sharedPreferences.getString("nik", null);
        idpersonal = sharedPreferences.getString("idpersonal", null);


        nama = sharedPreferences.getString("nama", null);
        email = sharedPreferences.getString("email", null);
        ponsel = sharedPreferences.getString("hp", null);
        alamat = sharedPreferences.getString("alamat", null);
        kd_posisi = sharedPreferences.getString("kd_posisi", null);
        posisi = sharedPreferences.getString("posisi", null);
        kd_pro = sharedPreferences.getString("kd_pro", null);
        provinsi = sharedPreferences.getString("provinsi", null);
        kd_kab = sharedPreferences.getString("kd_kab", null);
        kabupaten = sharedPreferences.getString("kabupaten", null);
        kd_kec = sharedPreferences.getString("kd_kec", null);
        kecamatan = sharedPreferences.getString("kecamatan", null);

        System.out.println(nama + provinsi + posisi);

        if (session) {
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            intent.putExtra("nik", nik);
            intent.putExtra("idpersonal", idpersonal);
            intent.putExtra("nama", nama);
            intent.putExtra("email", email);
            intent.putExtra("hp", ponsel);
            intent.putExtra("alamat", alamat);
            intent.putExtra("kd_posisi", kd_posisi);
            intent.putExtra("posisi", posisi);
            intent.putExtra("kd_pro", kd_pro);
            intent.putExtra("provinsi", provinsi);
            intent.putExtra("kd_kab", kd_kab);
            intent.putExtra("kabupaten", kabupaten);
            intent.putExtra("kd_kec", kd_kec);
            intent.putExtra("kecamatan", kecamatan);
            finish();
            startActivity(intent);
        }

        btn_login.setOnClickListener(this);
        registerTv.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {
            case R.id.btn_login:
                getValueLogin();
                break;
            case R.id.tv_register:
                startActivity(new Intent(this, PendaftaranActivity.class));
                break;
            default:
                break;
        }
    }

    private void getValueLogin() {
        username = username_et.getText().toString();
        password = password_et.getText().toString();

        if (conMgr.getActiveNetworkInfo() != null
                && conMgr.getActiveNetworkInfo().isAvailable()
                && conMgr.getActiveNetworkInfo().isConnected()) {
            getLogin(username, password);
        } else {
            Toast.makeText(getApplicationContext(), "Periksa koneksi internet",
                    Toast.LENGTH_LONG).show();
        }

    }

    private void getLogin(final String username, final String password) {
        progressDialog.setTitle("Login");
        progressDialog.setMessage("Tunggu Sebentar ...");
        progressDialog.show();

        StringRequest strReq = new StringRequest(Request.Method.POST, url, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //Log.e(TAG, "Login Response: " + response.toString());
                try {
                    JSONObject jObj = new JSONObject(response);
                    success = jObj.getBoolean("success");
                    progressDialog.dismiss();

                    // mengecek eror pada node json
                    if (success == true) {
                        String nik = jObj.getString("nik");
                        String idpersonal = jObj.getString("idpersonal");

                        Log.e("Successfully Login!", jObj.toString());
                        // Toast
                        Toast.makeText(getApplicationContext(), "Selamat Datang " + jObj.getString("nik"), Toast.LENGTH_LONG).show();

                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putBoolean(session_status, true);
                        editor.putString("nik", nik);
                        editor.putString("idpersonal", idpersonal);
                        finish();
                        editor.commit();

                        // memanggil main activity
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        intent.putExtra("nik", nik);
                        intent.putExtra("idpersonal", idpersonal);
                        intent.putExtra("nama", nama);
                        intent.putExtra("email", email);
                        intent.putExtra("hp", ponsel);
                        intent.putExtra("alamat", alamat);
                        intent.putExtra("kd_posisi", kd_posisi);
                        intent.putExtra("posisi", posisi);
                        intent.putExtra("kd_pro", kd_pro);
                        intent.putExtra("provinsi", provinsi);
                        intent.putExtra("kd_kab", kd_kab);
                        intent.putExtra("kabupaten", kabupaten);
                        intent.putExtra("kd_kec", kd_kec);
                        intent.putExtra("kecamatan", kecamatan);
                        finish();
                        startActivity(intent);
                        progressDialog.dismiss();
                    } else {
                        Toast.makeText(getApplicationContext(), jObj.getString("msg"),
                                Toast.LENGTH_LONG).show();
                        progressDialog.dismiss();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Log.e(TAG, "Login Eror: " + error.getMessage());
                Toast.makeText(getApplicationContext(), error.getMessage(),
                        Toast.LENGTH_LONG).show();
                progressDialog.dismiss();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("nik", username);
                params.put("password", password);
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(strReq, tag_jObj_obj);
    }
}
