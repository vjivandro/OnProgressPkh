package com.sdmpkh.kemensos.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by ivandro on 6/25/18.
 */

public class SharedPrefManager {

    public static final String KEMENSOS_APP = "kemensosapp";

    public static final String NIK = "nik";

    public static final String SUDAH_LOGIN = "SudahLogin";

    SharedPreferences sp;
    SharedPreferences.Editor spEditor;

    public SharedPrefManager(Context context){
        sp = context.getSharedPreferences(KEMENSOS_APP, Context.MODE_PRIVATE);
        spEditor = sp.edit();
    }

    public void saveSPString(String keySP, String value){
        spEditor.putString(keySP, value);
        spEditor.commit();
    }

    public void saveSPBoolean(String keySP, boolean value){
        spEditor.putBoolean(keySP, value);
        spEditor.commit();
    }

    public String getNik(){
        return sp.getString(NIK, "");
    }

    public Boolean getSudahLogin(){
        return sp.getBoolean(SUDAH_LOGIN, false);
    }
}
