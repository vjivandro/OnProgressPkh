package com.sdmpkh.kemensos.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sdmpkh.kemensos.R;
import com.sdmpkh.kemensos.model.PendidikanModel;

import java.util.List;

public class   PendidikanAdapter extends BaseAdapter {
    List<PendidikanModel> pendidikanModelList;
    Activity activity;
    LayoutInflater layoutInflater;

    public PendidikanAdapter(List<PendidikanModel> pendidikanModelList, Activity activity) {
        this.pendidikanModelList = pendidikanModelList;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return pendidikanModelList.size();
    }

    @Override
    public Object getItem(int position) {
        return pendidikanModelList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (layoutInflater == null) {
            layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_pendidikan, null);
        }

        TextView pendidikan, tahun, ipk;

        pendidikan = (TextView) convertView.findViewById(R.id.tv_pendidikan);
        tahun = (TextView) convertView.findViewById(R.id.tv_tahunLulus);
        ipk = (TextView) convertView.findViewById(R.id.tv_ipk);

        PendidikanModel data = pendidikanModelList.get(position);

        String thlulus = data.thlulus;
        String thmasuk = data.thmasuk;

        pendidikan.setText(data.kdtingkat);
        tahun.setText(thmasuk +"/"+ thlulus);
        ipk.setText(data.ipk);

        return convertView;
    }
}
