package com.sdmpkh.kemensos.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sdmpkh.kemensos.R;
import com.sdmpkh.kemensos.model.PrestasiModel;

import java.util.List;

public class PrestasiAdapter extends BaseAdapter {
    //List<PengalamanModel> pengalamanModelList;
    //    Activity activity;
    //    LayoutInflater layoutInflater;

    List<PrestasiModel> prestasiModelList;
    Activity activity;
    LayoutInflater layoutInflater;

    public PrestasiAdapter(List<PrestasiModel> prestasiModelList, Activity activity) {
        this.prestasiModelList = prestasiModelList;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return prestasiModelList.size();
    }

    @Override
    public Object getItem(int position) {
        return prestasiModelList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (layoutInflater == null) {
            layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_prestasi, null);
        }

        TextView namaprestasi, tahunprestasi;

        namaprestasi = (TextView) convertView.findViewById(R.id.tv_prestasi);
        tahunprestasi = (TextView) convertView.findViewById(R.id.tv_tahunprestasi);

        PrestasiModel data = prestasiModelList.get(position);

        namaprestasi.setText(data.nama_prestasi);
        tahunprestasi.setText(data.tahun);

        return convertView;
    }
}
