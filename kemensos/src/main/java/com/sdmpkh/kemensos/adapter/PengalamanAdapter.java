package com.sdmpkh.kemensos.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sdmpkh.kemensos.R;
import com.sdmpkh.kemensos.model.PengalamanModel;

import java.util.List;

public class PengalamanAdapter extends BaseAdapter {
    List<PengalamanModel> pengalamanModelList;
    Activity activity;
    LayoutInflater layoutInflater;

    public PengalamanAdapter(List<PengalamanModel> pengalamanModelList, Activity activity) {
        this.pengalamanModelList = pengalamanModelList;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return pengalamanModelList.size();
    }

    @Override
    public Object getItem(int position) {
        return pengalamanModelList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (layoutInflater == null) {
            layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_pengalaman, null);
        }

        TextView tahunKerja, jenisPenglaman;

        jenisPenglaman = (TextView) convertView.findViewById(R.id.tv_jenisPenglaman);
        tahunKerja = (TextView) convertView.findViewById(R.id.tv_tahun);

        PengalamanModel data = pengalamanModelList.get(position);

        String tahunkeluar = data.thkeluar;
        String tahunmasuk = data.thmasuk;

        jenisPenglaman.setText(data.jenis_pengalaman);
        tahunKerja.setText(tahunkeluar + "/" + tahunmasuk);
        return convertView;
    }
}
