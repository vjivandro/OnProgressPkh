package com.sdmpkh.kemensos.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sdmpkh.kemensos.R;
import com.sdmpkh.kemensos.model.PenghargaanModel;

import java.util.List;

public class PenghargaanAdapter extends BaseAdapter {
    List<PenghargaanModel> penghargaanModelList;
    Activity activity;
    LayoutInflater layoutInflater;

    public PenghargaanAdapter(List<PenghargaanModel> penghargaanModelList, Activity activity) {
        this.penghargaanModelList = penghargaanModelList;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return penghargaanModelList.size();
    }

    @Override
    public Object getItem(int position) {
        return penghargaanModelList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (layoutInflater == null) {
            layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_penghargaan, null);
        }

        TextView namapenghargaan, tahunpenghargaan;

        namapenghargaan = (TextView) convertView.findViewById(R.id.tv_penghargaan);
        tahunpenghargaan = (TextView) convertView.findViewById(R.id.tv_tahunPenghargaan);

        PenghargaanModel data = penghargaanModelList.get(position);

        namapenghargaan.setText(data.nama_penghargaan);
        tahunpenghargaan.setText(data.tahun);
        return convertView;
    }
}
