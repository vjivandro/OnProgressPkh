package com.sdmpkh.kemensos.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sdmpkh.kemensos.R;
import com.sdmpkh.kemensos.model.SertifikatModel;

import java.util.List;

public class SertifikatAdapter extends BaseAdapter {
    List<SertifikatModel> sertifikatModelList;
    Activity activity;
    LayoutInflater layoutInflater;

    public SertifikatAdapter(List<SertifikatModel> sertifikatModelList, Activity activity) {
        this.sertifikatModelList = sertifikatModelList;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return sertifikatModelList.size();
    }

    @Override
    public Object getItem(int position) {
        return sertifikatModelList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (layoutInflater == null) {
            layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_sertifikat, null);
        }

        TextView namasertifikat, tahunsertfikat;

        namasertifikat = (TextView) convertView.findViewById(R.id.tv_sertifikat);
        tahunsertfikat = (TextView) convertView.findViewById(R.id.tv_tahunsertifikat);

        SertifikatModel data = sertifikatModelList.get(position);

        namasertifikat.setText(data.nama_sertifikat);
        tahunsertfikat.setText(data.tahun);
        return convertView;
    }
}
