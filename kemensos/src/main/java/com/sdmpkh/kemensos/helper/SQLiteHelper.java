package com.sdmpkh.kemensos.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.sdmpkh.kemensos.model.DataKtpModel;
import com.sdmpkh.kemensos.model.DataPribadiModel;
import com.sdmpkh.kemensos.model.PendidikanModel;
import com.sdmpkh.kemensos.model.PengalamanModel;
import com.sdmpkh.kemensos.model.PenghargaanModel;
import com.sdmpkh.kemensos.model.PrestasiModel;
import com.sdmpkh.kemensos.model.RegionalModel;
import com.sdmpkh.kemensos.model.SertifikatModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by ivandro on 6/24/18.
 */

public class SQLiteHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "seleksisdm.db";
    private static final int DATABASE_VERSION = 4;

    //data diri
    public static final String TABLE_DATAPRIBADI = "datapribadi";
    public static final String COLUMN_IDA = "id";
    public static final String COLUMN_NAMA = "namalengkap";
    public static final String COLUMN_EMAIL = "email";
    public static final String COLUMN_PONSEL = "ponsel";
    public static final String COLUMN_ALAMAT = "alamat";
    public static final String COLUMN_KDPOSISI = "kdposisi";
    public static final String COLUMN_KDPROPINSI = "kdpropinsi";
    public static final String COLUMN_PROPINSI = "propinsi";
    public static final String COLUMN_KDKABUPATEN = "kdkabupaten";
    public static final String COLUMN_KABUPATEN = "kabupaten";
    public static final String COLUMN_KDKECAMATAN = "kdkecamatan"; // kebalik awal kecamatan
    public static final String COLUMN_KECAMATAN = "kecamatan";

    //ktp
    public static final String TABLE_DATAKTP = "dataktp";
    public static final String COLUMN_IDB = "id";
    public static final String COLUMN_NIK = "nik";
    public static final String COLUMN_TEMPAT_LAHIR = "tempatlahir";
    public static final String COLUMN_TANGGAL_LAHIR = "tanggallahir";
    public static final String COLUMN_KDJK = "kdjk";
    public static final String COLUMN_JK = "jk";
    public static final String COLUMN_KEBANGSAAN = "kebangsaan";
    public static final String COLUMN_STATUS = "status";
    public static final String COLUMN_PROPINSI_KTP = "propinsiKtp";
    public static final String COLUMN_KABUPATEN_KTP = "kabupatenKtp";
    public static final String COLUMN_KECAMATAN_KTP = "kecamatanKtp";

    //pendidikan
    public static final String TABLE_PENDIDIKAN = "pendidikan";
    public static final String COLUMN_IDC = "id";
    public static final String COLUMN_KDTINGKAT = "kode_tingkat";
    public static final String COLUMN_TINGKAT = "tingkat";
    public static final String COLUMN_SEKOLAH = "sekolah";
    public static final String COLUMN_PENJURUSAN = "penjurusan";
    public static final String COLUMN_LULUS = "thlulus";
    public static final String COLUMN_MASUK = "thmasuk";
    public static final String COLUMN_IPK = "ipk";

    //pengalaman kerja
    public static final String TABLE_PENGALAMAN = "pengalaman";
    public static final String COLUMN_IDD = "id";
    public static final String COLUMN_JENISPENGALAMAN = "jenis_pengalaman";
    public static final String COLUMN_THKELUAR = "thkeluar";
    public static final String COLUMN_THMASUK = "thmasuk";
    public static final String COLUMN_KETA= "keterangan";

    //Prestasi/Pencapaian
    public static final String TABLE_PRESTASI = "prestasi";
    public static final String COLUMN_IDE = "id";
    public static final String COLUMN_NAMAPRESTASI = "nama_prestasi";
    public static final String COLUMN_TAHUNA = "tahun";
    public static final String COLUMN_KETB = "keterangan";

    //penghargaan
    public static final String TABLE_PENGHARGAAN = "penghargaan";
    public static final String COLUMN_IDF = "id";
    public static final String COLUMN_NAMAPENGHARGAAN = "nama_penghargaan";
    public static final String COLUMN_TAHUNB = "tahun";
    public static final String COLUMN_KETC = "keterangan";

    //sertifikat
    public static final String TABLE_SERTIFIKAT= "sertifikat";
    public static final String COLUMN_IDG = "id";
    public static final String COLUMN_NAMASERTIFIKAT = "nama_sertifikat";
    public static final String COLUMN_TAHUNC = "tahun";
    public static final String COLUMN_KETD = "keterangan";

    // namalengkap, email, ponsel, alamat, kdposisi, kdpropinsi, propinsi, kdkabupaten, kdecamatan, kecamatan,
    // nik, tempatlahir, tanggalalhir, kdjk, jk, kebangsaan, status,
    // kode_tingkat, tingkat, sekolah, penjurusan, thlulus, thmasuk,
    // jenis_pengalaman, thkeluar, thmasuk, keterangan,
    // nama_prestasi, tahun, keterangan,
    // nama_penghargaan, tahun, keterangan,
    // nama_sertifikat, tahun, keterangan

    private static SQLiteHelper dbInstance;
    private static SQLiteDatabase database;
    private Context mContext;

    public SQLiteHelper (Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        final String SQL_DATA_PRIBADI = "CREATE TABLE " + TABLE_DATAPRIBADI + " (" +
                COLUMN_IDA + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_NAMA + " TEXT NOT NULL, " +
                COLUMN_EMAIL + " TEXT NOT NULL, " +
                COLUMN_PONSEL + " TEXT NOT NULL, " +
                COLUMN_ALAMAT + " TEXT NOT NULL, " +
                COLUMN_KDPOSISI + " TEXT NOT NULL, " +
                COLUMN_KDPROPINSI + " TEXT NOT NULL, " +
                COLUMN_PROPINSI + " TEXT NOT NULL, " +
                COLUMN_KDKABUPATEN + " TEXT NOT NULL, " +
                COLUMN_KABUPATEN + " TEXT NOT NULL, " +
                COLUMN_KDKECAMATAN + " TEXT NOT NULL," +
                COLUMN_KECAMATAN + " TEXT NOT NULL" +
                " )";

        db.execSQL(SQL_DATA_PRIBADI);

        final String SQL_DATA_KTP = "CREATE TABLE " + TABLE_DATAKTP + " (" +
                COLUMN_IDB + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_NIK + " TEXT NOT NULL, " +
                COLUMN_TANGGAL_LAHIR + " TEXT NOT NULL, " +
                COLUMN_TEMPAT_LAHIR + " TEXT NOT NULL, " +
                COLUMN_KDJK + " TEXT NOT NULL, " +
                COLUMN_JK + " TEXT NOT NULL, " +
                COLUMN_KEBANGSAAN + " TEXT NOT NULL, " +
                COLUMN_STATUS + " TEXT NOT NULL," +
                COLUMN_PROPINSI_KTP + " TEXT NOT NULL," +
                COLUMN_KABUPATEN_KTP + " TEXT NOT NULL," +
                COLUMN_KECAMATAN_KTP + " TEXT NOT NULL" +
                " )";

        db.execSQL(SQL_DATA_KTP);

        final String SQL_DATA_PENDIDIKAN = "CREATE TABLE " + TABLE_PENDIDIKAN + " (" +
                COLUMN_IDC + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_KDTINGKAT + " TEXT NOT NULL, " +
                COLUMN_TINGKAT + " TEXT NOT NULL, " +
                COLUMN_SEKOLAH + " TEXT NOT NULL, " +
                COLUMN_PENJURUSAN + " TEXT NOT NULL, " +
                COLUMN_MASUK + " TEXT NOT NULL, " +
                COLUMN_LULUS + " TEXT NOT NULL, " +
                COLUMN_IPK + " TEXT NOT NULL" +
                " )";

        db.execSQL(SQL_DATA_PENDIDIKAN);

        final String SQL_DATA_PENGALAMAN = "CREATE TABLE " + TABLE_PENGALAMAN + " (" +
                COLUMN_IDD + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_JENISPENGALAMAN + " TEXT NOT NULL, " +
                COLUMN_THKELUAR + " TEXT NOT NULL, " +
                COLUMN_THMASUK + " TEXT NOT NULL, " +
                COLUMN_KETA + " TEXT NOT NULL" +
                " )";

        db.execSQL(SQL_DATA_PENGALAMAN);

        final String SQL_TABLE_PRESTASI = "CREATE TABLE " + TABLE_PRESTASI+ " (" +
                COLUMN_IDE + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_NAMAPRESTASI + " TEXT NOT NULL, " +
                COLUMN_TAHUNA + " TEXT NOT NULL, " +
                COLUMN_KETB + " TEXT NOT NULL" +
                " )";

        db.execSQL(SQL_TABLE_PRESTASI);

        final String SQL_TABLE_PENGHARGAAN = "CREATE TABLE " + TABLE_PENGHARGAAN+ " (" +
                COLUMN_IDF + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_NAMAPENGHARGAAN + " TEXT NOT NULL, " +
                COLUMN_TAHUNB + " TEXT NOT NULL, " +
                COLUMN_KETC + " TEXT NOT NULL" +
                " )";

        db.execSQL(SQL_TABLE_PENGHARGAAN);

        final String SQL_TABLE_SERTIFIKAT = "CREATE TABLE " + TABLE_SERTIFIKAT+ " (" +
                COLUMN_IDG + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_NAMASERTIFIKAT + " TEXT NOT NULL, " +
                COLUMN_TAHUNC + " TEXT NOT NULL, " +
                COLUMN_KETD + " TEXT NOT NULL" +
                " )";

        db.execSQL(SQL_TABLE_SERTIFIKAT);

//        public void update(String id, String name, String email, String ponsel, String alamat) {
//            SQLiteDatabase database = this.getWritableDatabase();
//
//            String updateQuery = "UPDATE " + TABLE_DATAPRIBADI + " SET "
//                    + COLUMN_NAMA + "='" + name + "', "
//                    + COLUMN_EMAIL + "='" + email + "',"
//                    + COLUMN_PONSEL + "='" + ponsel + "',"
//                    + COLUMN_ALAMAT + "='" + alamat + "',"
//                    + " WHERE " + COLUMN_IDA + "= '" + id + "'";
//            Log.e("update sqlite ", updateQuery);
//            database.execSQL(updateQuery);
//            database.close();
//        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DATAPRIBADI);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DATAKTP);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PENDIDIKAN);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PENGALAMAN);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PRESTASI);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PENGHARGAAN);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SERTIFIKAT);
        onCreate(db);
    }

    // Semua Data Pribadri
    public DataPribadiModel getAllDataPribadi() {
        DataPribadiModel dataPribadiModel = new DataPribadiModel();
        database = this.getReadableDatabase();
        String queryDataPribadi = "SELECT * FROM " + TABLE_DATAPRIBADI;

        Cursor cursor = database.rawQuery(queryDataPribadi, null);
        if (cursor.moveToFirst()) {

            do {
                dataPribadiModel.id = cursor.getString(cursor.getColumnIndex(COLUMN_IDA));
                dataPribadiModel.nama = cursor.getString(cursor.getColumnIndex(COLUMN_NAMA));
                dataPribadiModel.email = cursor.getString(cursor.getColumnIndex(COLUMN_EMAIL));
                dataPribadiModel.ponsel = cursor.getString(cursor.getColumnIndex(COLUMN_PONSEL));
                dataPribadiModel.alamat = cursor.getString(cursor.getColumnIndex(COLUMN_ALAMAT));
                dataPribadiModel.kdposisi = cursor.getString(cursor.getColumnIndex(COLUMN_KDPOSISI));
                dataPribadiModel.kdpropinsi = cursor.getString(cursor.getColumnIndex(COLUMN_KDPROPINSI));
                dataPribadiModel.propinsi = cursor.getString(cursor.getColumnIndex(COLUMN_PROPINSI));
                dataPribadiModel.kdkabupaten = cursor.getString(cursor.getColumnIndex(COLUMN_KDKABUPATEN));
                dataPribadiModel.kabupaten = cursor.getString(cursor.getColumnIndex(COLUMN_KABUPATEN));
                dataPribadiModel.kdkecamatan = cursor.getString(cursor.getColumnIndex(COLUMN_KDKECAMATAN));
                dataPribadiModel.kecamatan = cursor.getString(cursor.getColumnIndex(COLUMN_KECAMATAN));
            } while (cursor.moveToNext());
        }

        cursor.close();
        database.close();
        return dataPribadiModel;
    }

    // Semua Data KTP
    public DataKtpModel getAllDataKtp() {
        DataKtpModel dataKtpModel = new DataKtpModel();
        database = this.getReadableDatabase();
        String queryDataKtp = "SELECT * FROM " + TABLE_DATAKTP;

        Cursor cursor = database.rawQuery(queryDataKtp, null);
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();

            dataKtpModel.id = cursor.getString(cursor.getColumnIndex(COLUMN_IDB));
            dataKtpModel.nik = cursor.getString(cursor.getColumnIndex(COLUMN_NIK));
            dataKtpModel.tempatlahir = cursor.getString(cursor.getColumnIndex(COLUMN_TEMPAT_LAHIR));
            dataKtpModel.tanggallahir = cursor.getString(cursor.getColumnIndex(COLUMN_TANGGAL_LAHIR));
            dataKtpModel.kdjenisKelamin = cursor.getString(cursor.getColumnIndex(COLUMN_KDJK));
            dataKtpModel.jenisKelamin = cursor.getString(cursor.getColumnIndex(COLUMN_JK));
            dataKtpModel.kebangsaan = cursor.getString(cursor.getColumnIndex(COLUMN_KEBANGSAAN));
            dataKtpModel.status = cursor.getString(cursor.getColumnIndex(COLUMN_STATUS));
            dataKtpModel.propinsiKtp = cursor.getString(cursor.getColumnIndex(COLUMN_PROPINSI_KTP));
            dataKtpModel.kabupatenKtp = cursor.getString(cursor.getColumnIndex(COLUMN_KABUPATEN_KTP));
            dataKtpModel.kecamatanKtp = cursor.getString(cursor.getColumnIndex(COLUMN_KECAMATAN_KTP));
        }

        cursor.close();
        return dataKtpModel;
    }

    // Semua Data Pendidikan
    public ArrayList<HashMap<String, String>> getPendidikanAll(){
        ArrayList<HashMap<String, String>> listPendidikan;
        listPendidikan = new ArrayList<HashMap<String, String>>();
        database = this.getWritableDatabase();
        String queryPendidikan = "SELECT * FROM " + TABLE_PENDIDIKAN;

        Cursor cursor = database.rawQuery(queryPendidikan, null);
        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put(COLUMN_IDC, cursor.getString(0));
                map.put(COLUMN_KDTINGKAT, cursor.getString(1));
                map.put(COLUMN_TINGKAT, cursor.getString(2));
                map.put(COLUMN_SEKOLAH, cursor.getString(3));
                map.put(COLUMN_PENJURUSAN, cursor.getString(4));
                map.put(COLUMN_LULUS, cursor.getString(5));
                map.put(COLUMN_MASUK, cursor.getString(6));
                map.put(COLUMN_IPK, cursor.getString(7));
                listPendidikan.add(map);
            } while (cursor.moveToNext());
        }

        cursor.close();
        return listPendidikan;
    }

    // Semua data pengalaman
    public ArrayList<HashMap<String, String>> getPengalamanAll() {
        ArrayList<HashMap<String, String>> listPengalaman;
        listPengalaman = new ArrayList<HashMap<String, String>>();
        database = this.getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_PENGALAMAN;

        Cursor cursor = database.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put(COLUMN_IDC, cursor.getString(0));
                map.put(COLUMN_JENISPENGALAMAN,cursor.getString(1));
                map.put(COLUMN_THKELUAR,cursor.getString(2));
                map.put(COLUMN_MASUK,cursor.getString(3));
                listPengalaman.add(map);
            } while (cursor.moveToNext());
        }

        database.close();
        return listPengalaman;
    }

    // Semua data prestasi
    public ArrayList<HashMap<String, String>> getAllPrestasi() {
        ArrayList<HashMap<String, String>> listPencapaian;
        listPencapaian = new ArrayList<HashMap<String, String>>();
        database = this.getReadableDatabase();
        String queryPrestasi = "SELECT * FROM " + TABLE_PRESTASI;

        Cursor cursor = database.rawQuery(queryPrestasi, null);

        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put(COLUMN_IDE, cursor.getString(0));
                map.put(COLUMN_NAMAPRESTASI, cursor.getString(1));
                map.put(COLUMN_TAHUNA, cursor.getString(2));
                map.put(COLUMN_KETB, cursor.getString(3));
                listPencapaian.add(map);
            } while (cursor.moveToNext());
        }

        database.close();
        return listPencapaian;
    }

    // Semua data penghargaan
    public ArrayList<HashMap<String, String>> getAllPenghargaan() {
        ArrayList<HashMap<String, String>> listPenghargaan;
        listPenghargaan = new ArrayList<HashMap<String, String>>();
        database = this.getReadableDatabase();
        String queryPenghargaan = "SELECT * FROM " + TABLE_PENGHARGAAN;

        Cursor cursor = database.rawQuery(queryPenghargaan, null);

        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put(COLUMN_IDF, cursor.getString(0));
                map.put(COLUMN_NAMAPENGHARGAAN, cursor.getString(1));
                map.put(COLUMN_TAHUNB, cursor.getString(2));
                map.put(COLUMN_KETC, cursor.getString(3));
                listPenghargaan.add(map);
            } while (cursor.moveToNext());
        }

        cursor.close();
        return listPenghargaan;
    }

    // Semua data sertifikat
    public ArrayList<HashMap<String, String>> getAllSertifikat() {
        ArrayList<HashMap<String, String>> listSertifikat;
        listSertifikat = new ArrayList<HashMap<String, String>>();
        database = this.getReadableDatabase();
        String querySertifikat = "SELECT * FROM " + TABLE_SERTIFIKAT;

        Cursor cursor = database.rawQuery(querySertifikat, null);

        if (cursor.moveToFirst()) {
            do {
                HashMap<String, String> map = new HashMap<String, String>();
                map.put(COLUMN_IDG, cursor.getString(0));
                map.put(COLUMN_NAMASERTIFIKAT, cursor.getString(1));
                map.put(COLUMN_TAHUNC, cursor.getString(2));
                map.put(COLUMN_KETD, cursor.getString(3));
                listSertifikat.add(map);
            } while (cursor.moveToNext());
        }

        cursor.close();
        return listSertifikat;
    }

    // insert data pribadi
    public void insertDataPribadi(DataPribadiModel dataPribadiModel) {
        SQLiteDatabase database = this.getReadableDatabase();
        ContentValues values = new ContentValues();

        values.put(COLUMN_IDA, dataPribadiModel.getId());
        values.put(COLUMN_NAMA, dataPribadiModel.getNama());
        values.put(COLUMN_EMAIL, dataPribadiModel.getEmail());
        values.put(COLUMN_PONSEL, dataPribadiModel.getPonsel());
        values.put(COLUMN_ALAMAT, dataPribadiModel.getAlamat());
        values.put(COLUMN_KDPOSISI, dataPribadiModel.getKdposisi());
        values.put(COLUMN_KDPROPINSI, dataPribadiModel.getKdpropinsi());
        values.put(COLUMN_PROPINSI, dataPribadiModel.getPropinsi());
        values.put(COLUMN_KDKABUPATEN, dataPribadiModel.getKdkabupaten());
        values.put(COLUMN_KABUPATEN, dataPribadiModel.getKabupaten());
        values.put(COLUMN_KDKECAMATAN, dataPribadiModel.getKdkecamatan());
        values.put(COLUMN_KECAMATAN, dataPribadiModel.getKecamatan());

        database.insert(TABLE_DATAPRIBADI, null, values);
        database.close();
    }

    public boolean updateDataPribadi(DataPribadiModel dataPribadiModel) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(COLUMN_IDA, dataPribadiModel.getId());
        values.put(COLUMN_NAMA, dataPribadiModel.getNama());
        values.put(COLUMN_EMAIL, dataPribadiModel.getEmail());
        values.put(COLUMN_PONSEL, dataPribadiModel.getPonsel());
        values.put(COLUMN_ALAMAT, dataPribadiModel.getAlamat());

        //update(String table, ContentValues values, String whereClause, String[] whereArgs)

        database.update(TABLE_DATAPRIBADI, values, COLUMN_IDA+" = ?", new String[]{dataPribadiModel.getId()});


        return  true;
    }
    
    // insert data ktp
    public void insertDataKtp(DataKtpModel dataKtpModel) {
        SQLiteDatabase database = this.getReadableDatabase();
        ContentValues values = new ContentValues();

        values.put(COLUMN_IDB, dataKtpModel.getId());
        values.put(COLUMN_NIK, dataKtpModel.getNik());
        values.put(COLUMN_TANGGAL_LAHIR, dataKtpModel.getTanggallahir());
        values.put(COLUMN_TEMPAT_LAHIR, dataKtpModel.getTempatlahir());
        values.put(COLUMN_KDJK, dataKtpModel.getKdjenisKelamin());
        values.put(COLUMN_JK, dataKtpModel.getJenisKelamin());
        values.put(COLUMN_KEBANGSAAN, dataKtpModel.getKebangsaan());
        values.put(COLUMN_STATUS, dataKtpModel.getStatus());
        values.put(COLUMN_PROPINSI_KTP, dataKtpModel.getPropinsiKtp());
        values.put(COLUMN_KABUPATEN_KTP, dataKtpModel.getKabupatenKtp());
        values.put(COLUMN_KECAMATAN_KTP, dataKtpModel.getKecamatanKtp());

        database.insert(TABLE_DATAKTP, null, values);
        database.close();
    }

    // insert data pendidikan
    public void insertPendidikan(PendidikanModel pendidikanModel) {
        SQLiteDatabase database = this.getReadableDatabase();
        ContentValues values = new ContentValues();

        values.put(COLUMN_KDTINGKAT, pendidikanModel.getKdtingkat());
        values.put(COLUMN_TINGKAT, pendidikanModel.getTingkat());
        values.put(COLUMN_SEKOLAH, pendidikanModel.getSekolah());
        values.put(COLUMN_PENJURUSAN, pendidikanModel.getPenjurusan());
        values.put(COLUMN_LULUS, pendidikanModel.getThlulus());
        values.put(COLUMN_MASUK, pendidikanModel.getThmasuk());
        values.put(COLUMN_IPK, pendidikanModel.getIpk());

        database.insert(TABLE_PENDIDIKAN, null, values);
        database.close();
    }

    // insert pengalaman
    public void insertPengalaman(PengalamanModel pengalamanModel) {
        SQLiteDatabase database = this.getReadableDatabase();
        ContentValues values = new ContentValues();

        values.put(COLUMN_JENISPENGALAMAN, pengalamanModel.getJenis_pengalaman());
        values.put(COLUMN_THKELUAR, pengalamanModel.getThkeluar());
        values.put(COLUMN_THMASUK, pengalamanModel.getThmasuk());
        values.put(COLUMN_KETA, pengalamanModel.getKeterangan());

        database.insert(TABLE_PENGALAMAN, null, values);
        database.close();
    }

    //insert penghargaan
    public void insertPenghargaan(PenghargaanModel penghargaanModel) {
        SQLiteDatabase database = this.getReadableDatabase();
        ContentValues values = new ContentValues();

        values.put(COLUMN_NAMAPENGHARGAAN, penghargaanModel.getNama_penghargaan());
        values.put(COLUMN_TAHUNB, penghargaanModel.getTahun());
        values.put(COLUMN_KETC, penghargaanModel.getKeterangan());

        database.insert(TABLE_PENGHARGAAN, null, values);
        database.close();
    }

    // insert prestasi
    public void insertPrestasi(PrestasiModel prestasiModel) {
        SQLiteDatabase database = this.getReadableDatabase();
        ContentValues values = new ContentValues();

        values.put(COLUMN_NAMAPRESTASI, prestasiModel.getNama_prestasi());
        values.put(COLUMN_TAHUNA, prestasiModel.getTahun());
        values.put(COLUMN_KETB, prestasiModel.getKeterangan());

        database.insert(TABLE_PRESTASI, null, values);
        database.close();
    }

    // insert sertifikat
    public void insertSertifikat(SertifikatModel sertifikatModel) {
        SQLiteDatabase database = this.getReadableDatabase();
        ContentValues values = new ContentValues();

        values.put(COLUMN_NAMASERTIFIKAT, sertifikatModel.getNama_sertifikat());
        values.put(COLUMN_TAHUNB, sertifikatModel.getTahun());
        values.put(COLUMN_KETD, sertifikatModel.getKeterangan());

        database.insert(TABLE_SERTIFIKAT, null, values);
        database.close();
    }


    // update data diri
//    public void update(String id, String name, String email, String ponsel, String alamat, String provinsi, String kabupaten, String kecamatan) {
//        SQLiteDatabase database = this.getWritableDatabase();
//
//        String updateQuery = "UPDATE " + TABLE_DATAPRIBADI + " SET "
//                + COLUMN_NAMA + "='" + name + "', "
//                + COLUMN_EMAIL + "='" + email + "'"
//                + COLUMN_PONSEL + "='" + ponsel + "'"
//                + COLUMN_ALAMAT + "='" + alamat + "'"
//                + COLUMN_PROPINSI + "='" + provinsi + "'"
//                + COLUMN_KABUPATEN + "='" + kabupaten + "'"
//                + COLUMN_KECAMATAN + "='" + kecamatan + "'"
//                + " WHERE " + COLUMN_IDA + "=" + "'" + id + "'";
//        Log.e("update sqlite ", updateQuery);
//        database.execSQL(updateQuery);
//        database.close();
//    }



//    public void update(String id, String name, String email, String ponsel, String alamat) {
//        SQLiteDatabase database = this.getWritableDatabase();
//
//        String updateQuery = "UPDATE " + TABLE_DATAPRIBADI + " SET "
//                + COLUMN_NAMA + "='" + name + "', "
//                + COLUMN_EMAIL + "='" + email + "',"
//                + COLUMN_PONSEL + "='" + ponsel + "',"
//                + COLUMN_ALAMAT + "='" + alamat + "',"
//                + " WHERE " + COLUMN_IDA + "= '" + id + "'";
//        Log.e("update sqlite ", updateQuery);
//        database.execSQL(updateQuery);
//        database.close();
//    }
}
