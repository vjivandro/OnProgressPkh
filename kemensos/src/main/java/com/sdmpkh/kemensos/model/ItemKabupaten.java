package com.sdmpkh.kemensos.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ivandro on 7/2/18.
 */

public class ItemKabupaten {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("ddlkabupaten")
    @Expose
    private List<KabupatenModel> ddlkabupaten = null;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public List<KabupatenModel> getDdlkabupaten() {
        return ddlkabupaten;
    }

    public void setDdlkabupaten(List<KabupatenModel> ddlkabupaten) {
        this.ddlkabupaten = ddlkabupaten;
    }
}
