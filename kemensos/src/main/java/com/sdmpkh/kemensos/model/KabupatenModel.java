package com.sdmpkh.kemensos.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ivandro on 7/2/18.
 */

public class KabupatenModel {
    @SerializedName("kode")
    @Expose
    private String kode;
    @SerializedName("kabupaten")
    @Expose
    private String kabupaten;

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getKabupaten() {
        return kabupaten;
    }

    public void setKabupaten(String kabupaten) {
        this.kabupaten = kabupaten;
    }
}
