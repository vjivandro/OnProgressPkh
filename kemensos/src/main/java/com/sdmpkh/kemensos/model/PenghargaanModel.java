package com.sdmpkh.kemensos.model;

public class PenghargaanModel {

    public String id;
    public String nama_penghargaan;
    public String tahun;
    public String keterangan;

    public PenghargaanModel(String nama_penghargaan, String tahun, String keterangan) {
        this.nama_penghargaan = nama_penghargaan;
        this.tahun = tahun;
        this.keterangan = keterangan;
    }

    public PenghargaanModel() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama_penghargaan() {
        return nama_penghargaan;
    }

    public void setNama_penghargaan(String nama_penghargaan) {
        this.nama_penghargaan = nama_penghargaan;
    }

    public String getTahun() {
        return tahun;
    }

    public void setTahun(String tahun) {
        this.tahun = tahun;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }
}
