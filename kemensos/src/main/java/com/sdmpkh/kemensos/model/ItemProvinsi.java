package com.sdmpkh.kemensos.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ivandro on 7/1/18.
 */

public class




ItemProvinsi {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("ddlprovinsi")
    @Expose
    private List<ProvinsiModel> ddlprovinsi = null;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public List<ProvinsiModel> getDdlprovinsi() {
        return ddlprovinsi;
    }

    public void setDdlprovinsi(List<ProvinsiModel> ddlprovinsi) {
        this.ddlprovinsi = ddlprovinsi;
    }
}
