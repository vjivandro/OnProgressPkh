package com.sdmpkh.kemensos.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ivandro on 7/2/18.
 */

public class KecamatanModel {
    @SerializedName("kdkecamatan")
    @Expose
    private String kdkecamatan;
    @SerializedName("kecamatan")
    @Expose
    private String kecamatan;
    @SerializedName("kdkabupaten")
    @Expose
    private String kdkabupaten;
    @SerializedName("provinsi")
    @Expose
    private Object provinsi;
    @SerializedName("kabupaten")
    @Expose
    private Object kabupaten;

    public String getKdkecamatan() {
        return kdkecamatan;
    }

    public void setKdkecamatan(String kdkecamatan) {
        this.kdkecamatan = kdkecamatan;
    }

    public String getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }

    public String getKdkabupaten() {
        return kdkabupaten;
    }

    public void setKdkabupaten(String kdkabupaten) {
        this.kdkabupaten = kdkabupaten;
    }

    public Object getProvinsi() {
        return provinsi;
    }

    public void setProvinsi(Object provinsi) {
        this.provinsi = provinsi;
    }

    public Object getKabupaten() {
        return kabupaten;
    }

    public void setKabupaten(Object kabupaten) {
        this.kabupaten = kabupaten;
    }
}
