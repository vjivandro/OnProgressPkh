package com.sdmpkh.kemensos.model;

/**
 * Created by ivandro on 6/25/18.
 */

public class DataKtpModel {
    public String id;
    public String nik;
    public String tempatlahir;
    public String tanggallahir;
    public String kdjenisKelamin;
    public String jenisKelamin;
    public String kebangsaan;
    public String status;
    public String propinsiKtp;
    public String kabupatenKtp;
    public String kecamatanKtp;

    public DataKtpModel(String id, String nik, String tempatlahir, String tanggallahir, String kdjenisKelamin, String jenisKelamin, String kebangsaan, String status, String propinsiKtp, String kabupatenKtp, String kecamatanKtp) {
        this.id = id;
        this.nik = nik;
        this.tempatlahir = tempatlahir;
        this.tanggallahir = tanggallahir;
        this.kdjenisKelamin = kdjenisKelamin;
        this.jenisKelamin = jenisKelamin;
        this.kebangsaan = kebangsaan;
        this.status = status;
        this.propinsiKtp = propinsiKtp;
        this.kabupatenKtp = kabupatenKtp;
        this.kecamatanKtp = kecamatanKtp;
    }

    public DataKtpModel() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public String getTempatlahir() {
        return tempatlahir;
    }

    public void setTempatlahir(String tempatlahir) {
        this.tempatlahir = tempatlahir;
    }

    public String getTanggallahir() {
        return tanggallahir;
    }

    public void setTanggallahir(String tanggallahir) {
        this.tanggallahir = tanggallahir;
    }

    public String getJenisKelamin() {
        return jenisKelamin;
    }

    public void setJenisKelamin(String jenisKelamin) {
        this.jenisKelamin = jenisKelamin;
    }

    public String getKebangsaan() {
        return kebangsaan;
    }

    public void setKebangsaan(String kebangsaan) {
        this.kebangsaan = kebangsaan;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getKdjenisKelamin() {
        return kdjenisKelamin;
    }

    public void setKdjenisKelamin(String kdjenisKelamin) {
        this.kdjenisKelamin = kdjenisKelamin;
    }

    public String getPropinsiKtp() {
        return propinsiKtp;
    }

    public void setPropinsiKtp(String propinsiKtp) {
        this.propinsiKtp = propinsiKtp;
    }

    public String getKabupatenKtp() {
        return kabupatenKtp;
    }

    public void setKabupatenKtp(String kabupatenKtp) {
        this.kabupatenKtp = kabupatenKtp;
    }

    public String getKecamatanKtp() {
        return kecamatanKtp;
    }

    public void setKecamatanKtp(String kecamatanKtp) {
        this.kecamatanKtp = kecamatanKtp;
    }
}
