package com.sdmpkh.kemensos.model;

public class PengalamanModel {
    public String id;
    public String jenis_pengalaman;
    public String thkeluar;
    public String thmasuk;
    public String keterangan;


    public PengalamanModel(String jenis_pengalaman, String thkeluar, String thmasuk, String keterangan) {
        this.jenis_pengalaman = jenis_pengalaman;
        this.thkeluar = thkeluar;
        this.thmasuk = thmasuk;
        this.keterangan = keterangan;
    }

    public PengalamanModel() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJenis_pengalaman() {
        return jenis_pengalaman;
    }

    public void setJenis_pengalaman(String jenis_pengalaman) {
        this.jenis_pengalaman = jenis_pengalaman;
    }

    public String getThkeluar() {
        return thkeluar;
    }

    public void setThkeluar(String thkeluar) {
        this.thkeluar = thkeluar;
    }

    public String getThmasuk() {
        return thmasuk;
    }

    public void setThmasuk(String thmasuk) {
        this.thmasuk = thmasuk;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }
}
