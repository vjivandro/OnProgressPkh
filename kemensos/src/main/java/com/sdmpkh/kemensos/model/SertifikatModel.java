package com.sdmpkh.kemensos.model;

public class SertifikatModel {
    public String sertifikat;
    public String id;
    public String nama_sertifikat;
    public String tahun;
    public String keterangan;

    public SertifikatModel(String nama_sertifikat, String tahun, String keterangan) {
        this.nama_sertifikat = nama_sertifikat;
        this.tahun = tahun;
        this.keterangan = keterangan;
    }

    public SertifikatModel() {
    }

    public String getSertifikat() {
        return sertifikat;
    }

    public void setSertifikat(String sertifikat) {
        this.sertifikat = sertifikat;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama_sertifikat() {
        return nama_sertifikat;
    }

    public void setNama_sertifikat(String nama_sertifikat) {
        this.nama_sertifikat = nama_sertifikat;
    }

    public String getTahun() {
        return tahun;
    }

    public void setTahun(String tahun) {
        this.tahun = tahun;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }
}
