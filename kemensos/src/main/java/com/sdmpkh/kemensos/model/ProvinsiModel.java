package com.sdmpkh.kemensos.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ivandro on 7/1/18.
 */

public class ProvinsiModel {
    @SerializedName("kode")
    @Expose
    private String kode;
    @SerializedName("provinsi")
    @Expose
    private String provinsi;

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getProvinsi() {
        return provinsi;
    }

    public void setProvinsi(String provinsi) {
        this.provinsi = provinsi;
    }
}
