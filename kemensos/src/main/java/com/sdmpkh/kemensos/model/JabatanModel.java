package com.sdmpkh.kemensos.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JabatanModel {
    @SerializedName("kdjabatan")
    @Expose
    private String kdjabatan;
    @SerializedName("jabatan")
    @Expose
    private String jabatan;

    public String getKdjabatan() {
        return kdjabatan;
    }

    public void setKdjabatan(String kdjabatan) {
        this.kdjabatan = kdjabatan;
    }

    public String getJabatan() {
        return jabatan;
    }

    public void setJabatan(String jabatan) {
        this.jabatan = jabatan;
    }
}
