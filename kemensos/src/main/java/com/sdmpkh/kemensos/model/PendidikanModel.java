package com.sdmpkh.kemensos.model;

/**
 * Created by ivandro on 6/29/18.
 */

public class PendidikanModel {
    public String id;
    public String kdtingkat;
    public String tingkat;
    public String sekolah;
    public String penjurusan;
    public String thlulus;
    public String thmasuk;
    public String ipk;

    public PendidikanModel(String kdtingkat, String tingkat, String sekolah, String penjurusan, String thlulus, String thmasuk, String ipk) {
        this.kdtingkat = kdtingkat;
        this.tingkat = tingkat;
        this.sekolah = sekolah;
        this.penjurusan = penjurusan;
        this.thlulus = thlulus;
        this.thmasuk = thmasuk;
        this.ipk = ipk;
    }

    public PendidikanModel() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKdtingkat() {
        return kdtingkat;
    }

    public void setKdtingkat(String kdtingkat) {
        this.kdtingkat = kdtingkat;
    }

    public String getTingkat() {
        return tingkat;
    }

    public void setTingkat(String tingkat) {
        this.tingkat = tingkat;
    }

    public String getSekolah() {
        return sekolah;
    }

    public void setSekolah(String sekolah) {
        this.sekolah = sekolah;
    }

    public String getPenjurusan() {
        return penjurusan;
    }

    public void setPenjurusan(String penjurusan) {
        this.penjurusan = penjurusan;
    }

    public String getThlulus() {
        return thlulus;
    }

    public void setThlulus(String thlulus) {
        this.thlulus = thlulus;
    }

    public String getThmasuk() {
        return thmasuk;
    }

    public void setThmasuk(String thmasuk) {
        this.thmasuk = thmasuk;
    }

    public String getIpk() {
        return ipk;
    }

    public void setIpk(String ipk) {
        this.ipk = ipk;
    }
}
