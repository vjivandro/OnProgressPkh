package com.sdmpkh.kemensos.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ivandro on 6/28/18.
 */

public class RegionalModel {
    @SerializedName("kode")
    @Expose
    private String kode;
    @SerializedName("regional")
    @Expose
    private String regional;

    public RegionalModel(String kode, String regional) {
        this.kode = kode;
        this.regional = regional;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getRegional() {
        return regional;
    }

    public void setRegional(String regional) {
        this.regional = regional;
    }
}
