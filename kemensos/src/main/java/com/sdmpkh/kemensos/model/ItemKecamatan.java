package com.sdmpkh.kemensos.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ivandro on 7/2/18.
 */

public class ItemKecamatan {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("ddldesa")
    @Expose
    private List<KecamatanModel> ddldesa = null;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public List<KecamatanModel> getDdldesa() {
        return ddldesa;
    }

    public void setDdldesa(List<KecamatanModel> ddldesa) {
        this.ddldesa = ddldesa;
    }
}
