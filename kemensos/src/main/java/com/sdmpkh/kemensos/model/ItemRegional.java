package com.sdmpkh.kemensos.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ivandro on 6/28/18.
 */

public class ItemRegional {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("ddlregional")
    @Expose
    private List<RegionalModel> ddlregional = null;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public List<RegionalModel> getDdlregional() {
        return ddlregional;
    }

    public void setDdlregional(List<RegionalModel> ddlregional) {
        this.ddlregional = ddlregional;
    }
}
