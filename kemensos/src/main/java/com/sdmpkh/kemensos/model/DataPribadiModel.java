package com.sdmpkh.kemensos.model;

import android.graphics.Bitmap;

/**
 * Created by ivandro on 6/24/18.
 */

public class DataPribadiModel {

    public String id;
    public String nama;
    public String email;
    public String ponsel;
    public String alamat;
    public String kdposisi;
    public String kdpropinsi;
    public String propinsi;
    public String kdkabupaten;
    public String kabupaten;
    public String kdkecamatan;
    public String kecamatan;

    public DataPribadiModel(String id, String nama, String email, String ponsel, String alamat, String kdposisi, String kdpropinsi, String propinsi, String kdkabupaten, String kabupaten, String kdkecamatan, String kecamatan) {
        this.id = id;
        this.nama = nama;
        this.email = email;
        this.ponsel = ponsel;
        this.alamat = alamat;
        this.kdposisi = kdposisi;
        this.kdpropinsi = kdpropinsi;
        this.propinsi = propinsi;
        this.kdkabupaten = kdkabupaten;
        this.kabupaten = kabupaten;
        this.kdkecamatan = kdkecamatan;
        this.kecamatan = kecamatan;
    }

    public DataPribadiModel(String nama, String email, String ponsel, String alamat) {
        this.nama = nama;
        this.email = email;
        this.ponsel = ponsel;
        this.alamat = alamat;
    }

    public DataPribadiModel() {

    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPonsel() {
        return ponsel;
    }

    public void setPonsel(String ponsel) {
        this.ponsel = ponsel;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getPropinsi() {
        return propinsi;
    }

    public void setPropinsi(String propinsi) {
        this.propinsi = propinsi;
    }

    public String getKabupaten() {
        return kabupaten;
    }

    public void setKabupaten(String kabupaten) {
        this.kabupaten = kabupaten;
    }

    public String getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }

    public String getKdposisi() {
        return kdposisi;
    }

    public void setKdposisi(String kdposisi) {
        this.kdposisi = kdposisi;
    }

    public String getKdpropinsi() {
        return kdpropinsi;
    }

    public void setKdpropinsi(String kdpropinsi) {
        this.kdpropinsi = kdpropinsi;
    }

    public String getKdkabupaten() {
        return kdkabupaten;
    }

    public void setKdkabupaten(String kdkabupaten) {
        this.kdkabupaten = kdkabupaten;
    }

    public String getKdkecamatan() {
        return kdkecamatan;
    }

    public void setKdkecamatan(String kdkecamatan) {
        this.kdkecamatan = kdkecamatan;
    }
}
