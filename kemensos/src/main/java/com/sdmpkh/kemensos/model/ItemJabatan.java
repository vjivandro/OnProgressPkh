package com.sdmpkh.kemensos.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ItemJabatan {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("ddljabatan")
    @Expose
    private List<JabatanModel> ddljabatan = null;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public List<JabatanModel> getDdljabatan() {
        return ddljabatan;
    }

    public void setDdljabatan(List<JabatanModel> ddljabatan) {
        this.ddljabatan = ddljabatan;
    }
}
