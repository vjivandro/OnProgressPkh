package com.sdmpkh.kemensos.model;

public class PrestasiModel {
    public String prestasi;
    public String id;
    public String nama_prestasi;
    public String tahun;
    public String keterangan;

    public PrestasiModel(String nama_prestasi, String tahun, String keterangan) {
        this.nama_prestasi = nama_prestasi;
        this.tahun = tahun;
        this.keterangan = keterangan;
    }

    public PrestasiModel() {
    }

    public String getPrestasi() {
        return prestasi;
    }

    public void setPrestasi(String prestasi) {
        this.prestasi = prestasi;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama_prestasi() {
        return nama_prestasi;
    }

    public void setNama_prestasi(String nama_prestasi) {
        this.nama_prestasi = nama_prestasi;
    }

    public String getTahun() {
        return tahun;
    }

    public void setTahun(String tahun) {
        this.tahun = tahun;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }
}
