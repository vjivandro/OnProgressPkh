package com.sdmpkh.kemensos.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.sdmpkh.kemensos.adapter.PendidikanAdapter;
import com.sdmpkh.kemensos.adapter.PengalamanAdapter;
import com.sdmpkh.kemensos.adapter.PenghargaanAdapter;
import com.sdmpkh.kemensos.adapter.PrestasiAdapter;
import com.sdmpkh.kemensos.R;
import com.sdmpkh.kemensos.activity.EditDataDiriActivity;
import com.sdmpkh.kemensos.activity.KtpEdit;
import com.sdmpkh.kemensos.activity.PendidikanEdit;
import com.sdmpkh.kemensos.activity.PengalamanKerja;
import com.sdmpkh.kemensos.activity.Penghargaan;
import com.sdmpkh.kemensos.activity.Prestasi;
import com.sdmpkh.kemensos.activity.Sertifikat;
import com.sdmpkh.kemensos.adapter.SertifikatAdapter;
import com.sdmpkh.kemensos.helper.SQLiteHelper;
import com.sdmpkh.kemensos.model.DataKtpModel;
import com.sdmpkh.kemensos.model.DataPribadiModel;
import com.sdmpkh.kemensos.model.PendidikanModel;
import com.sdmpkh.kemensos.model.PengalamanModel;
import com.sdmpkh.kemensos.model.PenghargaanModel;
import com.sdmpkh.kemensos.model.PrestasiModel;
import com.sdmpkh.kemensos.model.SertifikatModel;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import pl.aprilapps.easyphotopicker.EasyImage;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * create an instance of this fragment.
 */
public class ProfilFragment extends Fragment implements View.OnClickListener{
    private TextView ubah_tv, ubahKtp_tv, tv_tambahPendidikan, tv_penghargaan, tv_prestasi, tv_pengalaman, tv_sertifikat; //action
    private TextView nama_tv, email_tv, ponsel_tv, alamat_tv, propinsi_tv, kabupaten_tv, kecamatan_tv,
            viewFotoKtp_tv, viewFotoIjasah_tv, viewFotoDomisili_tv; // View Data Pribadi
    private TextView nik_tv, tempatlahir_tv, tanggallahir_tv, jeniskelamin_tv, kebangsaan_tv,
            status_tv, propinsiKtp_tv, kabupatenKtp_tv, kecamatanKtp_tv; // View Data Ktp
    private TextView tv_pendidikanksosong, tv_pengalamankosong, tv_pencapaiankosong, tv_penghargaankosong, tv_sertifikatkosong;
    private ConstraintLayout co_pendidikan, co_pengalaman, co_pencapaian, co_penghargaan, co_sertifikat;

    ListView pendidikanlist, pengalamanlist, pencapaianlist, penghargaanlist, sertifikatlist;
    List<PendidikanModel> listPendidikan;
    List<PengalamanModel> listPengalaman;
    List<PrestasiModel> listPrestasi;
    List<PenghargaanModel> listPenghargaan;
    List<SertifikatModel> listSertifikat;
    ArrayList<HashMap<String, String>> row_pendidikan, row_pengalaman, row_pencapaian, row_penghargaan, row_sertifikat;

    private ImageView myImage;
    private SQLiteHelper db;
    private String idpersonal, nik, nama, email, ponsel, alamat,
            kd_posisi, posisi, kd_pro, provinsi, kd_kab,
            kabupaten, kd_kec, kecamatan;

    private String [] items = {"Camera","Gallery"};

    public static final String my_shared_preferences = "my_shared_preferences";
    private SharedPreferences sharedPreferences;
    String pathKtp, pathIjasah, pathDomisili;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profil, container, false);

        sharedPreferences = getActivity().getSharedPreferences(my_shared_preferences, Context.MODE_PRIVATE);
        db = new SQLiteHelper(getActivity());

        ubah_tv = (TextView) view.findViewById(R.id.tv_ubah);
        ubahKtp_tv = (TextView) view.findViewById(R.id.tv_ubahKtp);
        tv_tambahPendidikan = (TextView) view.findViewById(R.id.tv_tambahPendidikan);
        tv_pengalaman = (TextView) view.findViewById(R.id.tv_tambahPengalaman);
        tv_penghargaan = (TextView) view.findViewById(R.id.tv_tambahPenghargaan);
        tv_prestasi = (TextView) view.findViewById(R.id.tv_tambahPrestasi);
        tv_sertifikat = (TextView) view.findViewById(R.id.tv_tambahSertifikat);

        ubah_tv.setOnClickListener(this);
        ubahKtp_tv.setOnClickListener(this);
        tv_tambahPendidikan.setOnClickListener(this);
        tv_pengalaman.setOnClickListener(this);
        tv_penghargaan.setOnClickListener(this);
        tv_prestasi.setOnClickListener(this);
        tv_sertifikat.setOnClickListener(this);

        DataDiri(view);
        Ktp(view);
        Pendidikan(view);
        Pengalaman(view);
        Pencapaian(view);
        Penghargaan(view);
        SertfikatAction(view);

        return view;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        switch (id) {
            case R.id.tv_ubah:
                startActivity(new Intent(getActivity(), EditDataDiriActivity.class));
                break;
            case R.id.tv_fotoktp:
                getDialogFotoKtp();
                break;
            case R.id.tv_fotoIjasah:
                getDialogFotoIjasah();
                break;
            case R.id.tv_ketDomisili:
                getDialogFotoKD();
                break;
            case R.id.tv_ubahKtp:
                startActivity(new Intent(getActivity(), KtpEdit.class));
                break;
            case R.id.tv_tambahPendidikan:
                startActivity(new Intent(getActivity(), PendidikanEdit.class));
                break;
            case R.id.tv_tambahPengalaman:
                startActivity(new Intent(getActivity(), PengalamanKerja.class));
                break;
            case R.id.tv_tambahPrestasi:
                startActivity(new Intent(getActivity(), Prestasi.class));
                break;
            case R.id.tv_tambahPenghargaan:
                startActivity(new Intent(getActivity(), Penghargaan.class));
                break;
            case R.id.tv_tambahSertifikat:
                startActivity(new Intent(getActivity(), Sertifikat.class));
                break;
        }
    }

    // Foto KTP
    private void getDialogFotoKtp() {

        AlertDialog.Builder alertadd = new AlertDialog.Builder(getContext());
        LayoutInflater factory = LayoutInflater.from(getContext());
        View view = factory.inflate(R.layout.image_dialog, null);

        myImage = (ImageView) view.findViewById(R.id.imageview);
        File file = new File(pathKtp);
        if (file.exists()) {
            Bitmap bp = BitmapFactory.decodeFile(file.getAbsolutePath());
            myImage.setImageBitmap(bp);
        }

        alertadd.setView(view);
        alertadd.setCancelable(true);
        alertadd.show();
    }

    // Foto Ijasah
    private void getDialogFotoIjasah() {

        AlertDialog.Builder alertadd = new AlertDialog.Builder(getContext());
        LayoutInflater factory = LayoutInflater.from(getContext());
        View view = factory.inflate(R.layout.image_dialog, null);

        myImage = (ImageView) view.findViewById(R.id.imageview);
        File file = new File(pathIjasah);
        if (file.exists()) {
            Bitmap bp = BitmapFactory.decodeFile(file.getAbsolutePath());
            myImage.setImageBitmap(bp);
        }

        alertadd.setView(view);
        alertadd.setCancelable(true);
        alertadd.show();
    }

    // Foto Keterangan Domisili
    private void getDialogFotoKD() {
        DataPribadiModel dataPribadiModel = db.getAllDataPribadi();

        AlertDialog.Builder alertadd = new AlertDialog.Builder(getContext());
        LayoutInflater factory = LayoutInflater.from(getContext());
        View view = factory.inflate(R.layout.image_dialog, null);

        myImage = (ImageView) view.findViewById(R.id.imageview);
        File file = new File(pathDomisili);
        if (file.exists()) {
            Bitmap bp = BitmapFactory.decodeFile(file.getAbsolutePath());
            myImage.setImageBitmap(bp);
        }
        alertadd.setView(view);
        alertadd.setCancelable(true);
        alertadd.show();
    }

    // Data Diri
    private void DataDiri(View view) {
        nama_tv = (TextView) view.findViewById(R.id.tv_namalengkap);
        email_tv = (TextView) view.findViewById(R.id.tv_emaildp);
        ponsel_tv = (TextView) view.findViewById(R.id.tv_Nope);
        alamat_tv = (TextView) view.findViewById(R.id.tv_alamat);
        propinsi_tv = (TextView) view.findViewById(R.id.tv_Provinsi);
        kabupaten_tv = (TextView) view.findViewById(R.id.tv_kabupatendp);
        kecamatan_tv = (TextView) view.findViewById(R.id.tv_kab);
        viewFotoKtp_tv = (TextView) view.findViewById(R.id.tv_fotoktp);
        viewFotoIjasah_tv = (TextView) view.findViewById(R.id.tv_fotoIjasah);
        viewFotoDomisili_tv = (TextView) view.findViewById(R.id.tv_ketDomisili);

        viewFotoKtp_tv.setTextColor(getResources().getColor(R.color.colorPrimary));
        viewFotoIjasah_tv.setTextColor(getResources().getColor(R.color.colorPrimary));
        viewFotoDomisili_tv.setTextColor(getResources().getColor(R.color.colorPrimary));
    }

    // Data KTP
    private void Ktp(View view) {
        nik_tv = (TextView) view.findViewById(R.id.tv_nik);
        tempatlahir_tv = (TextView) view.findViewById(R.id.tv_tempatLahir);
        tanggallahir_tv = (TextView) view.findViewById(R.id.tv_tanggallahir);
        jeniskelamin_tv = (TextView) view.findViewById(R.id.tv_jenisKelamin);
        kebangsaan_tv = (TextView) view.findViewById(R.id.tv_kebangsaan);
        status_tv = (TextView) view.findViewById(R.id.tv_status);
        propinsiKtp_tv = (TextView) view.findViewById(R.id.tv_ProvinsiKtp);
        kabupatenKtp_tv = (TextView) view.findViewById(R.id.tv_kabupatenKtp);
        kecamatanKtp_tv = (TextView) view.findViewById(R.id.tv_kecamatanKtp);

        String nik = sharedPreferences.getString("nik", "");

        nik_tv.setText(nik);
    }

    // Data Pendidikan
    private void Pendidikan(View view) {
        pendidikanlist = (ListView) view.findViewById(R.id.list_pendidikan);
        co_pendidikan = (ConstraintLayout) view.findViewById(R.id.constraint_pendidikan);
        tv_pendidikanksosong = (TextView) view.findViewById(R.id.tv_datakosong_pendidikan);

        getPendidikan();
    }

    // Data Pengalaman
    private void Pengalaman(View view) {
        pengalamanlist = (ListView) view.findViewById(R.id.list_pengalaman);
        co_pengalaman = (ConstraintLayout) view.findViewById(R.id.constraint_pengalaman);
        tv_pengalamankosong = (TextView) view.findViewById(R.id.tv_datakosong_pengalaman);

        getPengalaman();
    }

    // Data Prestasi
    private void Pencapaian(View view) {
        pencapaianlist = (ListView) view.findViewById(R.id.list_pencapaian);
        co_pencapaian = (ConstraintLayout) view.findViewById(R.id.constraint_pencapaian);
        tv_pencapaiankosong = (TextView) view.findViewById(R.id.tv_datakosong_pencapaian);

    }

    // Data Penghargaan
    private void Penghargaan(View view) {
        penghargaanlist = (ListView) view.findViewById(R.id.list_penghargaan);
        co_penghargaan = (ConstraintLayout) view.findViewById(R.id.constraint_penghargaan);
        tv_penghargaankosong = (TextView) view.findViewById(R.id.tv_datakosong_penghargaan);

    }

    // Data Sertifikat
    private void SertfikatAction(View view) {
        sertifikatlist = (ListView) view.findViewById(R.id.list_sertifikat);
        co_sertifikat = (ConstraintLayout) view.findViewById(R.id.constraint_sertifikat);
        tv_sertifikatkosong = (TextView) view.findViewById(R.id.tv_datakosong_sertifikat);

    }

    @Override
    public void onResume() {
        super.onResume();

        getDataDiri();
        getKTP();
        getPendidikan();
        getPengalaman();
        getPencapaian();
        getPenghargaan();
        getSertifikat();

        getFileImage();
    }

    private void getDataDiri() {
//        DataPribadiModel dataPribadiModel = db.getAllDataPribadi();
//        nama_tv.setText(dataPribadiModel.nama);
//        email_tv.setText(dataPribadiModel.email);
//        ponsel_tv.setText(dataPribadiModel.ponsel);
//        alamat_tv.setText(dataPribadiModel.alamat);
//        propinsi_tv.setText(dataPribadiModel.propinsi);
//        kabupaten_tv.setText(dataPribadiModel.kabupaten);
//        kecamatan_tv.setText(dataPribadiModel.kecamatan);

//        String idpersonal, nik, nama, email, ponsel, alamat, kd_posisi, posisi, kd_pro, provinsi, kd_kab, kabupaten, kd_kec, kecamatan;

        sharedPreferences = getActivity().getSharedPreferences(my_shared_preferences, Context.MODE_PRIVATE);
        nik = sharedPreferences.getString("nik", "");
        idpersonal = sharedPreferences.getString("idpersonal", "");
        nama = sharedPreferences.getString("nama", "");
        email = sharedPreferences.getString("email", "");
        ponsel = sharedPreferences.getString("hp", "");
        alamat = sharedPreferences.getString("alamat", "");
        kd_posisi = sharedPreferences.getString("kd_posisi", "");
        posisi = sharedPreferences.getString("posisi", "");
        kd_pro = sharedPreferences.getString("kd_pro", "");
        provinsi = sharedPreferences.getString("provinsi", "");
        kd_kab = sharedPreferences.getString("kd_kab", "");
        kabupaten = sharedPreferences.getString("kabupaten", "");
        kd_kec = sharedPreferences.getString("kd_kec", "");
        kecamatan = sharedPreferences.getString("kecamatan", "");

        nama_tv.setText(nama);
        email_tv.setText(email);
        ponsel_tv.setText(ponsel);
        alamat_tv.setText(alamat);
        propinsi_tv.setText(provinsi);
        kabupaten_tv.setText(kabupaten);
        kecamatan_tv.setText(kecamatan);

    }

    private void getKTP() {
        DataKtpModel dataKtpModel = db.getAllDataKtp();
//        nik_tv.setText(dataKtpModel.nik);
        tempatlahir_tv.setText(dataKtpModel.tempatlahir);
        tanggallahir_tv.setText(dataKtpModel.tanggallahir);
        jeniskelamin_tv.setText(dataKtpModel.jenisKelamin);
        kebangsaan_tv.setText(dataKtpModel.kebangsaan);
        status_tv.setText(dataKtpModel.status);
        propinsiKtp_tv.setText(dataKtpModel.propinsiKtp);
        kabupatenKtp_tv.setText(dataKtpModel.kabupatenKtp);
        kecamatanKtp_tv.setText(dataKtpModel.kecamatanKtp);
    }

    private void getPendidikan() {
        listPendidikan = new ArrayList<PendidikanModel>();
        row_pendidikan = db.getPendidikanAll();

        if (row_pendidikan.isEmpty()) {
            tv_pendidikanksosong.setVisibility(View.VISIBLE);
            co_pendidikan.setVisibility(View.GONE);
        } else {
            tv_pendidikanksosong.setVisibility(View.GONE);
            co_pendidikan.setVisibility(View.VISIBLE);

            for (int i = 0; i < row_pendidikan.size(); i++) {
                String tingkat = row_pendidikan.get(i).get("kode_tingkat");
                String tahunLulus = row_pendidikan.get(i).get("thlulus");
                String tahunMasuk = row_pendidikan.get(i).get("thmasuk");
                String ipk = row_pendidikan.get(i).get("ipk");

                PendidikanModel pendidikanModel = new PendidikanModel();
                pendidikanModel.setKdtingkat(tingkat);
                pendidikanModel.setThlulus(tahunLulus);
                pendidikanModel.setThmasuk(tahunMasuk);
                pendidikanModel.setIpk(ipk);

                listPendidikan.add(pendidikanModel);
            }
        }

        PendidikanAdapter adapter = new PendidikanAdapter(listPendidikan, getActivity());
        pendidikanlist.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private void getPengalaman() {
        listPengalaman = new ArrayList<PengalamanModel>();
        row_pengalaman = db.getPengalamanAll();

        if (row_pengalaman.isEmpty()) {
            tv_pengalamankosong.setVisibility(View.VISIBLE);
            co_pengalaman.setVisibility(View.GONE);
        } else {
            tv_pengalamankosong.setVisibility(View.GONE);
            co_pengalaman.setVisibility(View.VISIBLE);

            for (int i=0; i < row_pengalaman.size(); i++) {
                String jenis_penglaman = row_pengalaman.get(i).get("jenis_pengalaman");
                String tahun_masuk = row_pengalaman.get(i).get("thmasuk");
                String tahun_keluar = row_pengalaman.get(i).get("thkeluar");

                PengalamanModel pengalamanModel = new PengalamanModel();
                pengalamanModel.setJenis_pengalaman(jenis_penglaman);
                pengalamanModel.setThmasuk(tahun_masuk);
                pengalamanModel.setThkeluar(tahun_keluar);

                listPengalaman.add(pengalamanModel);
            }
        }

        PengalamanAdapter adapter = new PengalamanAdapter(listPengalaman, getActivity());
        pengalamanlist.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private void getPencapaian() {
        listPrestasi = new ArrayList<PrestasiModel>();
        row_pencapaian = db.getAllPrestasi();

        if (row_pencapaian.isEmpty()) {
            tv_pencapaiankosong.setVisibility(View.VISIBLE);
            co_pencapaian.setVisibility(View.GONE);
        } else {
            tv_pencapaiankosong.setVisibility(View.GONE);
            co_pencapaian.setVisibility(View.VISIBLE);

            for (int i=0; i < row_pencapaian.size(); i++) {
                String prestasi = row_pencapaian.get(i).get("nama_prestasi");
                String tahunprestasi = row_pencapaian.get(i).get("tahun");

                PrestasiModel prestasiModel = new PrestasiModel();
                prestasiModel.setNama_prestasi(prestasi);
                prestasiModel.setTahun(tahunprestasi);

                listPrestasi.add(prestasiModel);
            }
        }

        PrestasiAdapter adapter = new PrestasiAdapter(listPrestasi, getActivity());
        pencapaianlist.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private void getPenghargaan() {
        listPenghargaan = new ArrayList<PenghargaanModel>();
        row_penghargaan = db.getAllPenghargaan();

        if (row_penghargaan.isEmpty()){
            tv_penghargaankosong.setVisibility(View.VISIBLE);
            co_penghargaan.setVisibility(View.GONE);
        } else {
            tv_penghargaankosong.setVisibility(View.GONE);
            co_penghargaan.setVisibility(View.VISIBLE);

            for (int i=0; i < row_penghargaan.size(); i++) {
                String penghargaan = row_penghargaan.get(i).get("nama_penghargaan");
                String tahunPenghargaan = row_penghargaan.get(i).get("tahun");

                PenghargaanModel penghargaanModel = new PenghargaanModel();
                penghargaanModel.setNama_penghargaan(penghargaan);
                penghargaanModel.setTahun(tahunPenghargaan);

                listPenghargaan.add(penghargaanModel);
            }
        }

        PenghargaanAdapter adapter = new PenghargaanAdapter(listPenghargaan, getActivity());
        penghargaanlist.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private void getSertifikat() {
        listSertifikat = new ArrayList<SertifikatModel>();
        row_sertifikat = db.getAllSertifikat();

        if (row_sertifikat.isEmpty()){
            tv_sertifikatkosong.setVisibility(View.VISIBLE);
            co_sertifikat.setVisibility(View.GONE);
        } else {
            tv_sertifikatkosong.setVisibility(View.GONE);
            co_sertifikat.setVisibility(View.VISIBLE);

            for (int i=0; i<row_sertifikat.size(); i++) {
                String sertifikat = row_sertifikat.get(i).get("nama_sertifikat");
                String tahunSertifikat = row_sertifikat.get(i).get("tahun");

                SertifikatModel sertifikatModel = new SertifikatModel();
                sertifikatModel.setNama_sertifikat(sertifikat);
                sertifikatModel.setTahun(tahunSertifikat);

                listSertifikat.add(sertifikatModel);
            }
        }

        SertifikatAdapter adapter = new SertifikatAdapter(listSertifikat, getActivity());
        sertifikatlist.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private void getFileImage() {
        sharedPreferences = getActivity().getSharedPreferences(my_shared_preferences, Context.MODE_PRIVATE);

        pathKtp = sharedPreferences.getString("blobKtp", "");
        pathIjasah = sharedPreferences.getString("blobIjasah", "");
        pathDomisili = sharedPreferences.getString("blobDomisili", "");

        if (pathKtp.equals("")) {
            viewFotoKtp_tv.setText("Belum ada");
        } else {
            viewFotoKtp_tv.setText("Preview");

            viewFotoKtp_tv.setOnClickListener(this);
        }

        if (pathIjasah.equals("")) {
            viewFotoIjasah_tv.setText("Belum ada");
        } else {
            viewFotoIjasah_tv.setText("Preview");
            viewFotoIjasah_tv.setOnClickListener(this);
        }

        if (pathDomisili.equals("")) {
            viewFotoDomisili_tv.setText("Belum ada");
        } else {
            viewFotoDomisili_tv.setText("Preview");
            viewFotoDomisili_tv.setOnClickListener(this);
        }
    }

}
